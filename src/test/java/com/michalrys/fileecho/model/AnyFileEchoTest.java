package com.michalrys.fileecho.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AnyFileEchoTest {

    private FileEcho fileEcho;

    @Before
    public void setUp() {
        fileEcho = new AnyFileEcho();
    }

    @Test
    public void shouldSetFileWhenGivenFileIsCorrect() {
        //given
        File file = new File("./src/resources/toWebColor.txt");
        createExampleFile(file, "Example file,\nwith two lines.");
        fileEcho.setFile(file);

        //when
        File returnedFile = fileEcho.getFile();
        //then
        Assert.assertEquals(file, returnedFile);
        file.delete();
    }

    @Test
    public void shouldSetOnlyTheLastFileFromGivenSeveralCorrectFiles() {
        //given
        File fileA = new File("./src/resources/testA.txt");
        createExampleFile(fileA, "Example file,\nwith two lines.");

        File fileB = new File("./src/resources/testB.txt");
        createExampleFile(fileB, "Example file,\nwith two lines.");

        List<File> files = new ArrayList<>();
        files.add(fileA);
        files.add(fileB);
        fileEcho.setFile(files);

        //when
        File returnedFile = fileEcho.getFile();

        //then
        Assert.assertEquals(fileB, returnedFile);
        fileA.delete();
        fileB.delete();
    }

    @Test
    public void shouldSetNullWhenListOfFilesIsEmpty() {
        //given
        List<File> files = new ArrayList<>();
        fileEcho.setFile(files);

        //when
        File file = fileEcho.getFile();

        //then
        Assert.assertEquals(null, file);
    }

    @Test
    public void shouldSetNullIfFileIsIncorrect() {
        //given
        File fileNotExistA = new File("wrongFile");
        fileEcho.setFile(fileNotExistA);
        //when
        File file = fileEcho.getFile();

        //then
        Assert.assertEquals(null, file);
    }

    @Test
    public void shouldSetNullIfFileInFileListIsIncorrect() {
        //given
        File fileNotExistA = new File("wrongFile");
        File fileNotExistB = new File("wrongFile2");
        List<File> files = new ArrayList<>();
        files.add(fileNotExistA);
        files.add(fileNotExistB);
        fileEcho.setFile(files);
        //when
        File file = fileEcho.getFile();

        //then
        Assert.assertEquals(null, file);
    }

    @Test
    public void shouldStopReadingFileWhenStopReadingMethodIsCalled() {
        //given
        File file = new File("./src/resources/testA.txt");
        createExampleFile(file, "This is some text\nIn File at beginning");
        fileEcho.setFile(file);
        fileEcho.startReading();

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        fileEcho.stopReading();
        appendToExampleFile(file, "\nAdditional text.");

        //when
        StringBuffer status = fileEcho.getStatus();

        //then
        Assert.assertEquals("This is some text\nIn File at beginning", status.toString());
    }

    @Test
    public void shouldKeepReadingNonStopUntilStopReadingMethodIsCalled() {
        //given
        File file = new File("./src/resources/testA.txt");
        createExampleFile(file, "This is some text\nIn File at beginning");
        fileEcho.setFile(file);

        fileEcho.startReading();

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        appendToExampleFile(file, "\nAdditional text.");

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        fileEcho.stopReading();

        //when
        StringBuffer status = fileEcho.getStatus();

        //then
        Assert.assertEquals("This is some text\nIn File at beginning\nAdditional text.", status.toString());
    }

    @Test
    public void shouldPauseReadingUntilPausedOffIsCalled() {
        //given
        File file = new File("./src/resources/testA.txt");
        createExampleFile(file, "This is some text\nIn File at beginning");
        fileEcho.setFile(file);
        fileEcho.startReading();

        pause(100);
        appendToExampleFile(file, "\nAdditional text.");

        fileEcho.pauseReading(true);
        appendToExampleFile(file, "\nAdditional text after pause.");

        pause(100);
        fileEcho.pauseReading(false);

        pause(100);
        fileEcho.stopReading();

        //when
        StringBuffer status = fileEcho.getStatus();

        //then
        Assert.assertEquals("This is some text\nIn File at beginning\nAdditional text.\nAdditional text after pause.", status.toString());
    }

    @Test
    public void shouldClearStatus() {
        //given
        File file = new File("./src/resources/testA.txt");
        createExampleFile(file, "This is some text\nIn File at beginning");
        fileEcho.setFile(file);
        fileEcho.startReading();
        pause(100);
        fileEcho.stopReading();
        fileEcho.clearStatus();

        //when
        StringBuffer status = fileEcho.getStatus();

        //then
        Assert.assertEquals("", status.toString());
    }

    @Test
    public void shouldClearStatusPartly() {
        //given
        File file = new File("./src/resources/testA.txt");
        createExampleFile(file, "This is some text\nIn File at beginning");
        fileEcho.setFile(file);
        fileEcho.startReading();
        pause(100);
        appendToExampleFile(file, "\nAdditional text.");
        pause(100);
        fileEcho.clearStatus();
        pause(100);
        appendToExampleFile(file, "\nAdditional text after clear status.");
        pause(100);
        fileEcho.stopReading();

        //when
        StringBuffer status = fileEcho.getStatus();

        //then
        Assert.assertEquals("\nAdditional text after clear status.", status.toString());
    }

    private void createExampleFile(File file, String fileContent) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            bufferedWriter.write(fileContent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void appendToExampleFile(File file, String additionalText) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true))) {
            bufferedWriter.write(additionalText);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pause(int milisec) {
        try {
            Thread.sleep(milisec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
