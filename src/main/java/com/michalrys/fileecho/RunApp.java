package com.michalrys.fileecho;

import com.michalrys.fileecho.model.FileEcho;
import com.michalrys.fileecho.view.settings.GuiSettings;
import com.michalrys.fileecho.view.settings.JSONSettings;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Random;

public class RunApp extends Application {
    public static File file;
    public static Stage mainWindowStage;
    public static String userName = System.getProperty("user.name");
    public static GuiSettings settings;
    public static FileEcho model;
    public static String appPath;

    @Override
    public void start(Stage primaryStage) throws Exception {
        mainWindowStage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("view/mainWindow.fxml"));
        primaryStage.setScene(new Scene(root));

        if (file == null) {
            primaryStage.setTitle("FileEchoMR");
        }

        primaryStage.getIcons().add(new Image("file:./src/resources/icon.png"));
        primaryStage.setResizable(false);
        primaryStage.setAlwaysOnTop(true);
        primaryStage.setX(50 + getRandomCoordinate());
        primaryStage.setY(50 + getRandomCoordinate());
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.show();
    }


    public static void main(String[] args) {
        file = extractLastFileOnly(args);
        setAppPath();
        settings = new JSONSettings(appPath);

        launch(args);
    }

    private static File extractLastFileOnly(String[] args) {
        if (args.length != 0) {
            return new File(args[args.length - 1]);
        }
        return null;
    }

    public Stage getMainWindowStage() {
        return mainWindowStage;
    }

    public void setMainWindowStage(Stage mainWindowStage) {
        this.mainWindowStage = mainWindowStage;
    }

    private int getRandomCoordinate() {
        Random random = new Random();
        return 5 * random.nextInt(10);
    }

    private static void setAppPath() {
        String path = "";
        try {
            path = new File(RunApp.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getPath();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        //String lastPart = path.replaceFirst(".*\\\\", "");
        String lastPart = path.replaceFirst(".*\\\\", "");

        String[] foldersSplitted = path.split("\\\\");

        appPath = "";
        for(int i = 0; i <= foldersSplitted.length - 2; i++) {
            appPath += foldersSplitted[i] + "\\";
        }
        //appPath = path.replaceFirst(lastPart, "") + "\\";
    }
}