package com.michalrys.fileecho.view;

import com.michalrys.fileecho.MVC;
import com.michalrys.fileecho.RunApp;
import com.michalrys.fileecho.controller.FileEchoController;
import com.michalrys.fileecho.controller.SettingsController;
import com.michalrys.fileecho.model.AnyFileEcho;
import com.michalrys.fileecho.model.FileEcho;
import com.michalrys.fileecho.view.settings.GuiSettings;
import com.michalrys.fileecho.view.settings.SettingsObserver;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.*;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class MainWindowViewController implements Initializable, MVC.View, SettingsObserver.Observer {
    private FileEcho model;
    private MVC.Controller controller;
    private SettingsObserver.Controller controllerForSettings;
    private GuiSettings settings;
    public int buttonsOffset;
    final Delta dragDelta = new Delta();

    public static final String BUTTON_STOP = "Stop";
    public static final String BUTTON_START = "Start";
    public static final String BUTTON_PAUSE_ON = "Pause on";
    public static final String BUTTON_PAUSE_OFF = "Pause off";

    @FXML
    public Pane paneHead;
    @FXML
    public Label appStatus;
    @FXML
    public TextArea status;
    @FXML
    public Button buttonStopStart;
    @FXML
    public Button buttonPause;
    @FXML
    public Button buttonClear;
    @FXML
    public Button buttonSettings;
    @FXML
    public Button buttonReload;
    @FXML
    public Button buttonAuthor;
    @FXML
    public Button buttonMin;
    @FXML
    public Button buttonClose;

    public void setStatus(String newStatus) {
        status.setText(newStatus);
    }

    @Override
    public void update() {
        setStatus(model.getStatus().toString());
//        setStatus(RunApp.status);
        status.setScrollTop(Double.MAX_VALUE);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        model = new AnyFileEcho();
        model.addView(this);
        controller = new FileEchoController(model, this);
        RunApp.model = model;

        settings = RunApp.settings;
        settings.addView(this);
        controllerForSettings = new SettingsController(model, this, status, settings, buttonsOffset);
        updateMainWindowAndModelFromCurrentSettings();

        runWithInputArgs();
    }

    public void bStopStart(MouseEvent mouseEvent) {
        if (buttonStopStart.getText().equals(BUTTON_STOP)) {
            stopAfterStopClick();
        } else {
            runAfterStartClick();
        }
    }

    public void bPauseOnOff(MouseEvent mouseEvent) {
        if (buttonPause.getText().equals(BUTTON_PAUSE_OFF)) {
            pauseOn();
        } else {
            pauseOff();
        }
    }

    public void bClear(MouseEvent mouseEvent) {
        clearStatus();
    }

    public void bReaload(MouseEvent mouseEvent) {
        reload();
    }

    public void bAuthor(MouseEvent mouseEvent) {
        showAuthor();
    }

    public void bSettings(MouseEvent mouseEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("settingsWindow.fxml"));
            Parent rootFilters = fxmlLoader.load();
            Stage settingStage = new Stage();
            settingStage.setTitle("FileEchoSettings");

            settingStage.setScene(new Scene(rootFilters));
            settingStage.setX(RunApp.mainWindowStage.getX() + RunApp.mainWindowStage.getWidth());
            settingStage.setY(RunApp.mainWindowStage.getY());
            settingStage.initStyle(StageStyle.UNDECORATED);

            //IMPORTANT: here I pass some arguments to new stage
            SettingsWindowViewController controller = fxmlLoader.<SettingsWindowViewController>getController();
            controller.initDataFromMainWindow(controllerForSettings, buttonSettings, settingStage);

            settingStage.setAlwaysOnTop(true);
            settingStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void bMinimalize(MouseEvent mouseEvent) {
        RunApp.mainWindowStage.setIconified(true);
    }

    public void bClose(MouseEvent mouseEvent) {
        Platform.exit();
    }

    class Delta {
        double x, y;
    }

    public void paneHeadDragged(MouseEvent mouseEvent) {
        RunApp.mainWindowStage.setX(mouseEvent.getScreenX() + dragDelta.x);
        RunApp.mainWindowStage.setY(mouseEvent.getScreenY() + dragDelta.y);
    }

    public void paneHeadPressed(MouseEvent mouseEvent) {
        paneHead.setCursor(Cursor.CLOSED_HAND);
        dragDelta.x = RunApp.mainWindowStage.getX() - mouseEvent.getScreenX();
        dragDelta.y = RunApp.mainWindowStage.getY() - mouseEvent.getScreenY();
    }

    public void paneMouseReleased(MouseEvent mouseEvent) {
        paneHead.setCursor(Cursor.OPEN_HAND);
    }

    public void statusDragOver(DragEvent dragEvent) {
        dragEvent.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        dragEvent.consume();
    }

    public void statusDragDropped(DragEvent dragEvent) {
        Dragboard dragboard = dragEvent.getDragboard();
        List<File> files = dragboard.getFiles();
        dragEvent.setDropCompleted(true);
        dragEvent.consume();

        File file = extractLastFileOnly(files);
        RunApp.file = file;

        runAfterDragAndDrop();
    }

    private File extractLastFileOnly(List<File> files) {
        return files.get(files.size() - 1);
    }

    @Override
    public void runWithInputArgs() {
        controller.runWithInputArgs();
    }

    @Override
    public void runAfterDragAndDrop() {
        controller.runAfterDragAndDrop();
    }

    @Override
    public void runAfterStartClick() {
        controller.runAfterStartClick();
    }

    @Override
    public void stopAfterStopClick() {
        controller.stopAfterStopClick();
    }

    @Override
    public void pauseOn() {
        controller.pauseOn();
    }

    @Override
    public void pauseOff() {
        controller.pauseOff();
    }

    @Override
    public void clearStatus() {
        controller.clearStatus();
    }

    @Override
    public void reload() {
        controller.reload();
    }

    @Override
    public void showAuthor() {
        controller.showAuthor();
    }

    @Override
    public void setTitleByFile() {
        File file = RunApp.file;

        String absolutePath = file.getAbsolutePath();
        String[] fileNameSplitted = absolutePath.split("\\\\");
        StringBuffer newTitle = new StringBuffer();

        newTitle.append(fileNameSplitted[fileNameSplitted.length - 1]);
        newTitle.append(" <");
        for (int i = fileNameSplitted.length - 2; i >= 0; i--) {
            newTitle.append(fileNameSplitted[i] + "<");
        }

        if (newTitle.length() >= 30) {
            newTitle.substring(0, 30);
            newTitle.append("...");
        }

        RunApp.mainWindowStage.setTitle(newTitle.toString());
    }

    @Override
    public void updateSettings() {
        updateMainWindowAndModelFromCurrentSettings();
    }

    @Override
    public void reloadModel() {
        reload();
    }

    @Override
    public void updateMainWindowAndModelFromCurrentSettings() {
        controllerForSettings.updateMainWindowAndModelFromCurrentSettings();

//        Boolean settingsAlwaysOnTop = settings.getSettingBoolean(JSONSettings.KEY_WINDOW_ALLWAYS_ON_TOP);
//        RunApp.mainWindowStage.setAlwaysOnTop(settingsAlwaysOnTop);
//        String fontName = settings.getSettingString(JSONSettings.KEY_WINDOW_FONT_NAME);
//        int fontSize = settings.getSettingInteger(JSONSettings.KEY_WINDOW_FONT_SIZE);
//        status.setFont(new Font(fontName, fontSize));
//
//        String fontColorAwtString = settings.getSettingString(JSONSettings.KEY_WINDOW_FONT_COLOR);
//        String fontColorHex = ColorConversion.toWebColor(fontColorAwtString);
//        status.setStyle("-fx-text-fill: " + fontColorHex + ";");
//
//        model.setStatusLimit(settings.getSettingInteger(JSONSettings.KEY_STATUS_LINE_LIMIT));
//
//        //window
//        Integer windowW = settings.getSettingInteger(JSONSettings.KEY_WINDOW_WIDTH);
//        Integer windowH = settings.getSettingInteger(JSONSettings.KEY_WINDOW_HEIGHT);
//        RunApp.mainWindowStage.setWidth(windowW);
//        status.setPrefWidth(windowW - 10);
//        RunApp.mainWindowStage.setHeight(windowH);
//        status.setPrefHeight(windowH - 33);
//
//        String windowColorAwtString = settings.getSettingString(JSONSettings.KEY_WINDOW_COLOR);
//        String windowColorHex = ColorConversion.toWebColor(windowColorAwtString);
//        status.setStyle(status.getStyle() + "-fx-control-inner-background:" + windowColorHex + ";");
//
//        // contain lists - should contain
//        Object shouldContain = settings.getSetting(JSONSettings.KEY_ECHO_SHOULD_CONTAIN);
//        JSONObject shouldContainJsonObject = new JSONObject(shouldContain.toString());
//
//        String keyShouldContainActualList = settings.getSettingString(JSONSettings.KEY_ECHO_SHOULD_CONTAIN_ACTUAL);
//        Object shouldContainActualList = shouldContainJsonObject.get(keyShouldContainActualList);
//        JSONArray shouldContainActualListJsonArray = new JSONArray(shouldContainActualList.toString());
//
//        List<String> shouldContainActualMarkedList = new ArrayList<>();
//        for(Object o: shouldContainActualListJsonArray) {
//            shouldContainActualMarkedList.add(o.toString());
//        }
//
//        model.setShouldContainList(shouldContainActualMarkedList);
//        model.setShouldContainListName(keyShouldContainActualList);
//
////        System.out.println("shouldContainActualMarkedList --> " + shouldContainActualMarkedList);
////        System.out.println("keyShouldContainActualList --> " + keyShouldContainActualList);
//
//        // contain lists - should not contain
//        Object shouldNotContain = settings.getSetting(JSONSettings.KEY_ECHO_SHOULD_NOT_CONTAIN);
//        JSONObject shouldNotContainJsonObject = new JSONObject(shouldNotContain.toString());
//
//        String keyShouldNotContainActualList = settings.getSettingString(JSONSettings.KEY_ECHO_SHOULD_NOT_CONTAIN_ACTUAL);
//        Object shouldNotContainActualList = shouldNotContainJsonObject.get(keyShouldNotContainActualList);
//        JSONArray shouldNotContainActualListJsonArray = new JSONArray(shouldNotContainActualList.toString());
//
//        List<String> shouldNotContainActualMarkedList = new ArrayList<>();
//        for(Object o: shouldNotContainActualListJsonArray) {
//            shouldNotContainActualMarkedList.add(o.toString());
//        }
//
//        model.setShouldNotContainList(shouldNotContainActualMarkedList);
//        model.setShouldNotContainListName(keyShouldNotContainActualList);
//
////        System.out.println("shouldNotContainActualMarkedList --> " + shouldNotContainActualMarkedList);
////        System.out.println("keyShouldNotContainActualList --> " + keyShouldNotContainActualList);
    }
}