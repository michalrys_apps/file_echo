package com.michalrys.fileecho.view;

import com.michalrys.fileecho.RunApp;
import com.michalrys.fileecho.view.settings.ColorConversion;
import com.michalrys.fileecho.view.settings.GuiSettings;
import com.michalrys.fileecho.view.settings.JSONSettings;
import com.michalrys.fileecho.view.settings.SettingsObserver;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.*;

public class SettingsWindowViewController implements Initializable {
    private GuiSettings settings;
    private SettingsObserver.Controller controllerForSettings;

    private Button buttonSettings;
    private Stage settingStage;
    final Delta dragDelta = new Delta();

    @FXML
    public Pane settingsPane;
    @FXML
    public CheckBox settingAOT;
    @FXML
    public TextField fontName;
    @FXML
    public TextField fontSize;
    @FXML
    public Button fontPlus;
    @FXML
    public Button fontMinus;
    @FXML
    public TextField linesLimit;
    @FXML
    public Button fontDefault;
    @FXML
    public ColorPicker fontColor;
    //--------------------------
    @FXML
    public TextField windowW;
    @FXML
    public TextField windowH;
    @FXML
    public Button windowWPlus;
    @FXML
    public Button windowWMinus;
    @FXML
    public Button windowHPlus;
    @FXML
    public Button windowHMinus;
    @FXML
    public Button windowDefault;
    @FXML
    public ColorPicker windowColor;
    //---------------------------
    @FXML
    public TextArea containTextArea;
    @FXML
    private ChoiceBox containChoiceBox;
    @FXML
    private Button containNew;
    @FXML
    public Button containDel;
    @FXML
    public TextArea notContainTextArea;

    public void initDataFromMainWindow(SettingsObserver.Controller controllerForSettings, Button buttonSettings, Stage settingStage) {
        this.controllerForSettings = controllerForSettings;
        this.buttonSettings = buttonSettings;
        this.settingStage = settingStage;

        buttonSettings.setDisable(true);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        settings = RunApp.settings;

        fontName.setText(settings.getSettingString(JSONSettings.KEY_WINDOW_FONT_NAME));
        setContextMenuToFontName();
        fontSize.setText(settings.getSettingInteger(JSONSettings.KEY_WINDOW_FONT_SIZE).toString());
        Color fontColorJavaFX = ColorConversion.toJavaFXColor(settings.getSettingString(JSONSettings.KEY_WINDOW_FONT_COLOR));
        fontColor.setValue(fontColorJavaFX);

        linesLimit.setText(settings.getSettingInteger(JSONSettings.KEY_STATUS_LINE_LIMIT).toString());

        // window
        windowW.setText(settings.getSettingInteger(JSONSettings.KEY_WINDOW_WIDTH).toString());
        windowH.setText(settings.getSettingInteger(JSONSettings.KEY_WINDOW_HEIGHT).toString());
        Color windowColorJavaFX = ColorConversion.toJavaFXColor(settings.getSettingString(JSONSettings.KEY_WINDOW_COLOR));
        windowColor.setValue(windowColorJavaFX);

        // should contain list
        setShouldContainSettings();
    }

    private void setShouldContainSettings() {
        String shouldContainActualListName = settings.getSettingString(JSONSettings.KEY_ECHO_SHOULD_CONTAIN_ACTUAL);
        Object shouldContainLists = settings.getSetting(JSONSettings.KEY_ECHO_SHOULD_CONTAIN);
        JSONObject shouldContainListsJsonObject = new JSONObject(shouldContainLists.toString());
        Object shouldContainActualListObject = shouldContainListsJsonObject.get(shouldContainActualListName);

        String shouldContainActualListPrint = shouldContainActualListObject.toString().replaceAll("[\\]\\[\"]", "");
        shouldContainActualListPrint = shouldContainActualListPrint.replaceAll(",", "\n");
        containTextArea.setText(shouldContainActualListPrint);

        Set<String> setOfLists = shouldContainListsJsonObject.keySet();
        List<String> listOfLists = new ArrayList<>();
        listOfLists.addAll(setOfLists);
        listOfLists.sort(String::compareTo);
        int actualListId = listOfLists.indexOf(shouldContainActualListName);


        containChoiceBox.setItems(FXCollections.observableArrayList(listOfLists));
        containChoiceBox.getSelectionModel().select(actualListId);
        containChoiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                Object containListsString = settings.getSetting(JSONSettings.KEY_ECHO_SHOULD_CONTAIN);
                JSONObject containListsJsonObject = new JSONObject(containListsString.toString());
                Set<String> containListsSets = containListsJsonObject.keySet();
                List<String> listOfLists = new ArrayList<>();
                listOfLists.addAll(containListsSets);
                listOfLists.sort(String::compareTo);

                String selectedList = listOfLists.get(newValue.intValue());

                Object shouldContainLists = settings.getSetting(JSONSettings.KEY_ECHO_SHOULD_CONTAIN);
                JSONObject shouldContainListsJsonObject = new JSONObject(shouldContainLists.toString());
                Object shouldContainActualListObject = shouldContainListsJsonObject.get(selectedList);

                String shouldContainActualListPrint = shouldContainActualListObject.toString().replaceAll("[\\]\\[\"]", "");
                shouldContainActualListPrint = shouldContainActualListPrint.replaceAll("[ ]*,[ ]*", "\n");
                containTextArea.setText(shouldContainActualListPrint);
            }
        });
    }

    private void setContextMenuToFontName() {
        MenuItem fontArialNarrow = new MenuItem("Arial Narrow");
        fontArialNarrow.setOnAction(event -> fontName.setText(fontArialNarrow.getText()));
        MenuItem fontArialBold = new MenuItem("Arial Bold");
        fontArialBold.setOnAction(event -> fontName.setText(fontArialBold.getText()));
        MenuItem fontConsolas = new MenuItem("Consolas");
        fontConsolas.setOnAction(event -> fontName.setText(fontConsolas.getText()));
        MenuItem fontTahoma = new MenuItem("Tahoma");
        fontTahoma.setOnAction(event -> fontName.setText(fontTahoma.getText()));
        MenuItem fontCalibri = new MenuItem("Calibri");
        fontCalibri.setOnAction(event -> fontName.setText(fontCalibri.getText()));
        MenuItem fontAgencyFB = new MenuItem("Agency FB");
        fontAgencyFB.setOnAction(event -> fontName.setText(fontAgencyFB.getText()));
        MenuItem fontOldEnglish = new MenuItem("Old English Text MT");
        fontOldEnglish.setOnAction(event -> fontName.setText(fontOldEnglish.getText()));

        ContextMenu contextMenu = new ContextMenu(fontArialNarrow, fontArialBold, fontConsolas, fontTahoma,
                fontCalibri, fontAgencyFB, fontOldEnglish);
        fontName.setContextMenu(contextMenu);
    }

    class Delta {
        double x, y;
    }

    public void paneSettingsDragged(MouseEvent mouseEvent) {
        settingsPane.setCursor(Cursor.CLOSED_HAND);
        settingStage.setX(mouseEvent.getScreenX() + dragDelta.x);
        settingStage.setY(mouseEvent.getScreenY() + dragDelta.y);
    }

    public void paneSettingsPressed(MouseEvent mouseEvent) {
        dragDelta.x = settingStage.getX() - mouseEvent.getScreenX();
        dragDelta.y = settingStage.getY() - mouseEvent.getScreenY();
    }

    public void paneMouseReleased(MouseEvent mouseEvent) {
        settingsPane.setCursor(Cursor.OPEN_HAND);
    }

    public void actionAOT(ActionEvent actionEvent) {
        if (!settingAOT.isSelected()) {
            settingAOT.setSelected(false);
            settings.setSettingBoolean(JSONSettings.KEY_WINDOW_ALLWAYS_ON_TOP, false);
            settings.writeCurrentSettingsToDefaultFile();
            settings.notifyAllViewsForNewSettings();
        } else {
            settingAOT.setSelected(true);
            settings.setSettingBoolean(JSONSettings.KEY_WINDOW_ALLWAYS_ON_TOP, true);
            settings.writeCurrentSettingsToDefaultFile();
            settings.notifyAllViewsForNewSettings();
        }
    }

    public void clickedExit(MouseEvent mouseEvent) {
        buttonSettings.setDisable(false);
        settingStage.close();
    }

    public void clickedSaveAndUpdate(MouseEvent mouseEvent) {
        settings.setSettingString(JSONSettings.KEY_WINDOW_FONT_NAME, fontName.getText());
        settings.setSettingInteger(JSONSettings.KEY_WINDOW_FONT_SIZE, Integer.valueOf(fontSize.getText()));

        Color fontColorJavaFx = fontColor.getValue();
        java.awt.Color fontColorAwt = ColorConversion.toAwtColor(fontColorJavaFx);
        settings.setSettingString(JSONSettings.KEY_WINDOW_FONT_COLOR, fontColorAwt.toString());

        settings.setSettingInteger(JSONSettings.KEY_STATUS_LINE_LIMIT, Integer.valueOf(linesLimit.getText()));

        // window
        settings.setSettingInteger(JSONSettings.KEY_WINDOW_WIDTH, Integer.valueOf(windowW.getText()));
        settings.setSettingInteger(JSONSettings.KEY_WINDOW_HEIGHT, Integer.valueOf(windowH.getText()));

        Color windowColorJavaFx = windowColor.getValue();
        java.awt.Color windowColorAwt = ColorConversion.toAwtColor(windowColorJavaFx);
        settings.setSettingString(JSONSettings.KEY_WINDOW_COLOR, windowColorAwt.toString());

        // should contain list
        String selectedShouldContainList = (String) containChoiceBox.getValue();
        String textShouldContain = containTextArea.getText();

        List<String> textShouldContainAsList = new ArrayList<>();

        textShouldContainAsList.addAll(Arrays.asList(textShouldContain.split("\n")));

        System.out.println("List to save: ");
        for (String word : textShouldContainAsList) {
            System.out.println(">" + word + "<");
        }

        Object shouldContainLists = settings.getSetting(JSONSettings.KEY_ECHO_SHOULD_CONTAIN);
        JSONObject shouldContainListsJsonObject = new JSONObject(shouldContainLists.toString());

        shouldContainListsJsonObject.put(selectedShouldContainList, new JSONArray(textShouldContainAsList));
        settings.setSettingJSONObject(JSONSettings.KEY_ECHO_SHOULD_CONTAIN, shouldContainListsJsonObject);
        settings.setSettingString(JSONSettings.KEY_ECHO_SHOULD_CONTAIN_ACTUAL, selectedShouldContainList);

        // writing
        settings.writeCurrentSettingsToDefaultFile();
        settings.notifyAllViewsForNewSettings();
        settings.notifyAllViewsForReloadModel(); //TODO check behaviour if I need this here
    }

    public void actionFontColor(ActionEvent actionEvent) {
        Color colorJavaFx = fontColor.getValue();
        java.awt.Color colorAwtString = ColorConversion.toAwtColor(colorJavaFx);
        settings.setSettingString(JSONSettings.KEY_WINDOW_FONT_COLOR, colorAwtString.toString());
        settings.writeCurrentSettingsToDefaultFile();
        settings.notifyAllViewsForNewSettings();
    }

    public void clickedFontPlus(MouseEvent mouseEvent) {
        int fontSizeInt;
        try {
            fontSizeInt = Integer.valueOf(fontSize.getText());
            fontSizeInt++;
            settings.setSettingInteger(JSONSettings.KEY_WINDOW_FONT_SIZE, fontSizeInt);
            fontSize.setText(String.valueOf(fontSizeInt));
            settings.writeCurrentSettingsToDefaultFile();
            settings.notifyAllViewsForNewSettings();
        } catch (Exception e) {
        }
    }

    public void clickedFontMinus(MouseEvent mouseEvent) {
        int fontSizeInt;
        try {
            fontSizeInt = Integer.valueOf(fontSize.getText());
            fontSizeInt--;
            settings.setSettingInteger(JSONSettings.KEY_WINDOW_FONT_SIZE, fontSizeInt);
            fontSize.setText(String.valueOf(fontSizeInt));
            settings.writeCurrentSettingsToDefaultFile();
            settings.notifyAllViewsForNewSettings();
        } catch (Exception e) {
        }
    }

    public void scrollFontSize(ScrollEvent scrollEvent) {
        double deltaY = scrollEvent.getDeltaY();
        Integer value = Integer.valueOf(fontSize.getText());
        if (deltaY > 0) {
            value = Integer.valueOf(fontSize.getText()) + 1;
        } else if (deltaY < 0) {
            if (value >= 2) {
                value = Integer.valueOf(fontSize.getText()) - 1;
            }
        }

        fontSize.setText(value.toString());
        settings.setSettingInteger(JSONSettings.KEY_WINDOW_FONT_SIZE, Integer.valueOf(fontSize.getText()));
        settings.writeCurrentSettingsToDefaultFile();
        settings.notifyAllViewsForNewSettings();
    }

    public void clickedFontDefault(MouseEvent mouseEvent) {
        fontName.setText(settings.getDefaultSettings().getString(JSONSettings.KEY_WINDOW_FONT_NAME));
        fontSize.setText(String.valueOf(settings.getDefaultSettings().getInt(JSONSettings.KEY_WINDOW_FONT_SIZE)));

        String colorTest = settings.getDefaultSettings().get(JSONSettings.KEY_WINDOW_FONT_COLOR).toString();
        java.awt.Color cFD = JSONSettings.getColorFromColorAsString(colorTest);

        fontColor.setValue(Color.rgb(cFD.getRed(), cFD.getGreen(), cFD.getBlue()));

        linesLimit.setText(String.valueOf(settings.getDefaultSettings().getInt(JSONSettings.KEY_STATUS_LINE_LIMIT)));
    }

    public void pressedKeyFontName(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER)) {
            settings.setSettingString(JSONSettings.KEY_WINDOW_FONT_NAME, fontName.getText());
            settings.writeCurrentSettingsToDefaultFile();
            settings.notifyAllViewsForNewSettings();
        }
    }

    public void pressedKeyFontSize(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER)) {

            try {
                if (Integer.valueOf(fontSize.getText()) <= 0) {
                    fontSize.setText("1");
                }
            } catch (NumberFormatException e) {
                fontSize.setText(String.valueOf(settings.getDefaultSettings().getInt(JSONSettings.KEY_WINDOW_FONT_SIZE)));
            }

            settings.setSettingInteger(JSONSettings.KEY_WINDOW_FONT_SIZE, Integer.valueOf(fontSize.getText()));
            settings.writeCurrentSettingsToDefaultFile();
            settings.notifyAllViewsForNewSettings();
        }
    }

    public void pressedKeyLinesLimit(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER)) {
            try {
                if (Integer.valueOf(linesLimit.getText()) <= 0) {
                    linesLimit.setText("1");
                }
            } catch (NumberFormatException e) {
                linesLimit.setText(String.valueOf(settings.getDefaultSettings().getInt(JSONSettings.KEY_STATUS_LINE_LIMIT)));
            }

            settings.setSettingInteger(JSONSettings.KEY_STATUS_LINE_LIMIT, Integer.valueOf(linesLimit.getText()) - 1);
            settings.writeCurrentSettingsToDefaultFile();
            settings.notifyAllViewsForNewSettings();
            settings.notifyAllViewsForReloadModel();
        }
    }

    public void scrollLinesLimit(ScrollEvent scrollEvent) {
        double deltaY = scrollEvent.getDeltaY();
        Integer linesLimitCurrent = Integer.valueOf(linesLimit.getText());
        Integer value = 0;

        if (deltaY > 0) {
            if (linesLimitCurrent <= 200) {
                value = linesLimitCurrent + 1;
            } else {
                value = linesLimitCurrent;
            }
        } else if (deltaY < 0) {
            if (linesLimitCurrent >= 2) {
                value = linesLimitCurrent - 1;
            } else {
                value = 1;
            }
        }

        linesLimit.setText(value.toString());
        settings.setSettingInteger(JSONSettings.KEY_STATUS_LINE_LIMIT, Integer.valueOf(linesLimit.getText()) - 1);
        settings.writeCurrentSettingsToDefaultFile();
        settings.notifyAllViewsForNewSettings();
        settings.notifyAllViewsForReloadModel();
    }

    // window
    public void actionWindowColor(ActionEvent actionEvent) {
        Color colorJavaFx = windowColor.getValue();
        java.awt.Color colorAwtString = ColorConversion.toAwtColor(colorJavaFx);
        settings.setSettingString(JSONSettings.KEY_WINDOW_COLOR, colorAwtString.toString());
        settings.writeCurrentSettingsToDefaultFile();
        settings.notifyAllViewsForNewSettings();
    }

    public void clickedWindowWPlus(MouseEvent mouseEvent) {
        int windowSizeWCurrent;
        try {
            windowSizeWCurrent = Integer.valueOf(windowW.getText());
            windowSizeWCurrent++;
            settings.setSettingInteger(JSONSettings.KEY_WINDOW_WIDTH, windowSizeWCurrent);
            windowW.setText(String.valueOf(windowSizeWCurrent));
            settings.writeCurrentSettingsToDefaultFile();
            settings.notifyAllViewsForNewSettings();
        } catch (Exception e) {
        }
    }

    public void clickedWindowWMinus(MouseEvent mouseEvent) {
        int windowSizeWCurrent;
        try {
            windowSizeWCurrent = Integer.valueOf(windowW.getText());
            windowSizeWCurrent--;
            settings.setSettingInteger(JSONSettings.KEY_WINDOW_WIDTH, windowSizeWCurrent);
            windowW.setText(String.valueOf(windowSizeWCurrent));
            settings.writeCurrentSettingsToDefaultFile();
            settings.notifyAllViewsForNewSettings();
        } catch (Exception e) {
        }
    }

    public void clickedWindowHPlus(MouseEvent mouseEvent) {
        int windowSizeHCurrent;
        try {
            windowSizeHCurrent = Integer.valueOf(windowH.getText());
            windowSizeHCurrent++;
            settings.setSettingInteger(JSONSettings.KEY_WINDOW_HEIGHT, windowSizeHCurrent);
            windowH.setText(String.valueOf(windowSizeHCurrent));
            settings.writeCurrentSettingsToDefaultFile();
            settings.notifyAllViewsForNewSettings();
        } catch (Exception e) {
        }
    }

    public void clickedWindowHMinus(MouseEvent mouseEvent) {
        int windowSizeHCurrent;
        try {
            windowSizeHCurrent = Integer.valueOf(windowH.getText());
            windowSizeHCurrent--;
            settings.setSettingInteger(JSONSettings.KEY_WINDOW_HEIGHT, windowSizeHCurrent);
            windowH.setText(String.valueOf(windowSizeHCurrent));
            settings.writeCurrentSettingsToDefaultFile();
            settings.notifyAllViewsForNewSettings();
        } catch (Exception e) {
        }
    }

    public void clickedWindowDefault(MouseEvent mouseEvent) {
        Integer windowWDefault = settings.getDefaultSettings().getInt(JSONSettings.KEY_WINDOW_WIDTH);
        Integer windowHDefault = settings.getDefaultSettings().getInt(JSONSettings.KEY_WINDOW_HEIGHT);
        String windowColorDefault = settings.getDefaultSettings().getString(JSONSettings.KEY_WINDOW_COLOR);

        windowW.setText(windowWDefault.toString());
        windowH.setText(windowHDefault.toString());

        java.awt.Color cFD = JSONSettings.getColorFromColorAsString(windowColorDefault);
        windowColor.setValue(Color.rgb(cFD.getRed(), cFD.getGreen(), cFD.getBlue()));
        settings.setSettingInteger(JSONSettings.KEY_WINDOW_BUTTONS_OFFSET,
                settings.getDefaultSettings().getInt(JSONSettings.KEY_WINDOW_BUTTONS_OFFSET));
    }

    public void pressedKeyWindowW(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER)) {
            settings.setSettingInteger(JSONSettings.KEY_WINDOW_WIDTH, Integer.valueOf(windowW.getText()));
            rebuildWindowAccordingToWindowW();
            settings.writeCurrentSettingsToDefaultFile();
            settings.notifyAllViewsForNewSettings();
        }
    }

    public void pressedKeyWindowH(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER)) {
            settings.setSettingInteger(JSONSettings.KEY_WINDOW_HEIGHT, Integer.valueOf(windowH.getText()));
            settings.writeCurrentSettingsToDefaultFile();
            settings.notifyAllViewsForNewSettings();
        }
    }

    public void scrollWindowW(ScrollEvent scrollEvent) {
        double deltaY = scrollEvent.getDeltaY();
        Integer value = 0;
        if (deltaY > 0) {
            value = Integer.valueOf(windowW.getText()) + 10;
        } else if (deltaY < 0) {
            value = Integer.valueOf(windowW.getText()) - 10;
        }

        windowW.setText(value.toString());
        settings.setSettingInteger(JSONSettings.KEY_WINDOW_WIDTH, Integer.valueOf(windowW.getText()));
        rebuildWindowAccordingToWindowW();
        settings.writeCurrentSettingsToDefaultFile();
        settings.notifyAllViewsForNewSettings();
    }

    private void rebuildWindowAccordingToWindowW() {
        int width;
        try {
            width = Integer.valueOf(windowW.getText());
        } catch (Exception e) {
            return;
        }

        if (width >= 130 && width <= 620) {
            int deltaX = width - 620;
            controllerForSettings.setTranslateMainWindowButtons(deltaX);
        }
    }

    public void scrollWindowH(ScrollEvent scrollEvent) {
        double deltaY = scrollEvent.getDeltaY();
        Integer value = 0;
        if (deltaY > 0) {
            value = Integer.valueOf(windowH.getText()) + 10;
        } else if (deltaY < 0) {
            value = Integer.valueOf(windowH.getText()) - 10;
        }

        windowH.setText(value.toString());
        settings.setSettingInteger(JSONSettings.KEY_WINDOW_HEIGHT, Integer.valueOf(windowH.getText()));
        settings.writeCurrentSettingsToDefaultFile();
        settings.notifyAllViewsForNewSettings();
    }

    // contain
    public void scrollContainChoiceBox(ScrollEvent scrollEvent) {
        double deltaY = scrollEvent.getDeltaY();
        if (deltaY > 0) {
            containChoiceBox.getSelectionModel().selectPrevious();
        } else if (deltaY < 0) {
            containChoiceBox.getSelectionModel().selectNext();
        }
    }

    public void containNew(MouseEvent mouseEvent) {
        TextInputDialog dialog = new TextInputDialog("my name");
        dialog.setTitle("New should contain list");
        dialog.setHeaderText("Please enter name for a new list of words which should be contained.");
        dialog.setContentText("Name: ");
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.toFront();
        stage.setAlwaysOnTop(true);
        stage.setX(settingStage.getX());
        stage.setY(settingStage.getY() + 120);

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            String newListName = result.get();
            System.out.println("New list was created: " + newListName);

            Object containListsObject = settings.getSetting(JSONSettings.KEY_ECHO_SHOULD_CONTAIN);
            JSONObject containListsJsonObject = new JSONObject(containListsObject.toString());

            containListsJsonObject.put(newListName, new JSONArray().put("write here something - seperated by enter"));
            settings.setSettingJSONObject(JSONSettings.KEY_ECHO_SHOULD_CONTAIN, containListsJsonObject);

            containChoiceBox.getItems().add(newListName);

            settings.writeCurrentSettingsToDefaultFile();
            settings.notifyAllViewsForNewSettings();

            //FIXME here is problem :( -------
            String shouldContainActualListName = settings.getSettingString(JSONSettings.KEY_ECHO_SHOULD_CONTAIN_ACTUAL);
            Object shouldContainLists = settings.getSetting(JSONSettings.KEY_ECHO_SHOULD_CONTAIN);
            JSONObject shouldContainListsJsonObject = new JSONObject(shouldContainLists.toString());
            Object shouldContainActualListObject = shouldContainListsJsonObject.get(shouldContainActualListName);

            String shouldContainActualListPrint = shouldContainActualListObject.toString().replaceAll("[\\]\\[\"]", "");
            shouldContainActualListPrint = shouldContainActualListPrint.replaceAll(",", "\n");
            containTextArea.setText(shouldContainActualListPrint);

            Set<String> setOfLists = shouldContainListsJsonObject.keySet();
            List<String> listOfLists = new ArrayList<>();
            listOfLists.addAll(setOfLists);
            listOfLists.sort(String::compareTo);
            int actualListId = listOfLists.indexOf(newListName);


            containChoiceBox.setItems(FXCollections.observableArrayList(listOfLists));
            containChoiceBox.getSelectionModel().select(actualListId);
            containChoiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    Object containListsString = settings.getSetting(JSONSettings.KEY_ECHO_SHOULD_CONTAIN);
                    JSONObject containListsJsonObject = new JSONObject(containListsString.toString());
                    Set<String> containListsSets = containListsJsonObject.keySet();
                    List<String> listOfLists = new ArrayList<>();
                    listOfLists.addAll(containListsSets);
                    listOfLists.sort(String::compareTo);

                    String selectedList = listOfLists.get(newValue.intValue());

                    Object shouldContainLists = settings.getSetting(JSONSettings.KEY_ECHO_SHOULD_CONTAIN);
                    JSONObject shouldContainListsJsonObject = new JSONObject(shouldContainLists.toString());
                    Object shouldContainActualListObject = shouldContainListsJsonObject.get(selectedList);

                    String shouldContainActualListPrint = shouldContainActualListObject.toString().replaceAll("[\\]\\[\"]", "");
                    shouldContainActualListPrint = shouldContainActualListPrint.replaceAll("[ ]*,[ ]*", "\n");
                    containTextArea.setText(shouldContainActualListPrint);
                }
            });
            //FIXME here is problem :( -------
        }

    }

    public void containDel(MouseEvent mouseEvent) {
        //FIXME here is also problem
        int deleteIndex = containChoiceBox.getSelectionModel().getSelectedIndex();
        String deletedList = (String) containChoiceBox.getSelectionModel().getSelectedItem();
        containChoiceBox.getItems().remove(deleteIndex);
        containChoiceBox.getSelectionModel().selectFirst();
        String newActualList = (String) containChoiceBox.getSelectionModel().getSelectedItem();

        Object containListsObject = settings.getSetting(JSONSettings.KEY_ECHO_SHOULD_CONTAIN);
        JSONObject containListsJsonObject = new JSONObject(containListsObject.toString());
        containListsJsonObject.remove(deletedList);

        settings.setSettingJSONObject(JSONSettings.KEY_ECHO_SHOULD_CONTAIN, containListsJsonObject);
        settings.setSettingString(JSONSettings.KEY_ECHO_SHOULD_CONTAIN_ACTUAL, newActualList);
        settings.writeCurrentSettingsToDefaultFile();
    }
}