package com.michalrys.fileecho.view.settings;

import jdk.nashorn.internal.runtime.JSONListAdapter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class JSONSettings implements GuiSettings {
    private List<SettingsObserver.Observer> views = new ArrayList<>();

    private JSONObject settings;
    private JSONObject defaultSettings;
    private String fileName = "FileEchoMRSettings.json";
    private String filePath; //previously there was always on D:\

    //DEFAULT SETTING:
    public static final String KEY_WINDOW_ALLWAYS_ON_TOP = "Window always on top";
    public static final boolean VAL_WINDOW_ALLWAYS_ON_TOP = true;

    public static final String KEY_WINDOW_FONT_NAME = "Window font name";
    public static final String VAL_WINDOW_FONT_NAME = "Arial Narrow";

    public static final String KEY_WINDOW_FONT_SIZE = "Window font size";
    public static final int VAL_WINDOW_FONT_SIZE = 10;

    public static final String KEY_WINDOW_FONT_COLOR = "Window font color";
    public static final String VAL_WINDOW_FONT_COLOR = Color.BLACK.toString();

    public static final String KEY_STATUS_LINE_LIMIT = "Status line limit";
    public static final int VAL_STATUS_LINE_LIMIT = 28;

    public static final String KEY_WINDOW_WIDTH = "Window size width";
    public static final double VAL_WINDOW_WIDTH = 620;

    public static final String KEY_WINDOW_HEIGHT = "Window size height";
    public static final double VAL_WINDOW_HEIGHT = 455;

    public static final String KEY_WINDOW_COLOR = "Window color";
    public static final String VAL_WINDOW_COLOR = Color.WHITE.toString();

    public static final String KEY_ECHO_SHOULD_CONTAIN = "Echo should contain lists";
    public static JSONObject valEchoShouldContain;

    public static final String KEY_ECHO_SHOULD_CONTAIN_ACTUAL = "Actual contain list";
    public static String VAL_ECHO_SHOULD_CONTAIN_ACTUAL = "f06";

    public static final String KEY_ECHO_SHOULD_NOT_CONTAIN = "Echo should not contain lists";
    public static JSONObject valEchoShouldNotContain;

    public static final String KEY_ECHO_SHOULD_NOT_CONTAIN_ACTUAL = "Actual not contain list";
    public static String VAL_ECHO_SHOULD_NOT_CONTAIN_ACTUAL = "f06";

    public static final String KEY_WINDOW_BUTTONS_OFFSET = "Upper buttons offset";
    public static final int VAL_WINDOW_BUTTONS_OFFSET = 0;

    public JSONSettings(String filePath) {
        generateDefaultSettings();
        setDefaultSettings();
        this.filePath = filePath + fileName;

        if (doesFileSettingsExist(this.filePath)) {
            setSettingsFromFile(this.filePath);
        } else {
            settings = defaultSettings;
        }
        writeSettingsToFile(this.filePath);
    }

    @Override
    public boolean doesFileSettingsExist(String filePath) {
        return new File(filePath).exists();
    }

    @Override
    public void generateDefaultSettings() {
        defaultSettings = new JSONObject();
        defaultSettings.put(KEY_WINDOW_ALLWAYS_ON_TOP, VAL_WINDOW_ALLWAYS_ON_TOP);
        defaultSettings.put(KEY_WINDOW_FONT_NAME, VAL_WINDOW_FONT_NAME);
        defaultSettings.put(KEY_WINDOW_FONT_COLOR, VAL_WINDOW_FONT_COLOR);
        defaultSettings.put(KEY_WINDOW_FONT_SIZE, VAL_WINDOW_FONT_SIZE);
        defaultSettings.put(KEY_WINDOW_COLOR, VAL_WINDOW_COLOR);
        defaultSettings.put(KEY_WINDOW_WIDTH, VAL_WINDOW_WIDTH);
        defaultSettings.put(KEY_WINDOW_HEIGHT, VAL_WINDOW_HEIGHT);
        defaultSettings.put(KEY_STATUS_LINE_LIMIT, VAL_STATUS_LINE_LIMIT);

        valEchoShouldContain = new JSONObject();
        valEchoShouldContain.put("f06", new JSONArray(Arrays.asList(" ", "a", "b")));
        valEchoShouldContain.put("out", new JSONArray(Arrays.asList("a", "b")));
        valEchoShouldContain.put("sts", new JSONArray(Arrays.asList("%", ".")));
        defaultSettings.put(KEY_ECHO_SHOULD_CONTAIN, valEchoShouldContain);

        valEchoShouldNotContain = new JSONObject();
        valEchoShouldNotContain.put("f06", new JSONArray(Arrays.asList("-", "* * *", "MAXIMUM")));
        valEchoShouldNotContain.put("out", new JSONArray(Arrays.asList("x", "z")));
        valEchoShouldNotContain.put("sts", new JSONArray(Arrays.asList("1", "2", "3")));
        defaultSettings.put(KEY_ECHO_SHOULD_NOT_CONTAIN, valEchoShouldNotContain);

        defaultSettings.put(KEY_ECHO_SHOULD_CONTAIN_ACTUAL, VAL_ECHO_SHOULD_CONTAIN_ACTUAL);
        defaultSettings.put(KEY_ECHO_SHOULD_NOT_CONTAIN_ACTUAL, VAL_ECHO_SHOULD_NOT_CONTAIN_ACTUAL);

        defaultSettings.put(KEY_WINDOW_BUTTONS_OFFSET, VAL_WINDOW_BUTTONS_OFFSET);
    }

    //TODO test only
    private List<String> getStringListFromContainContenerFromDefaultSetting(String key, String jsonArrayKey) {
        JSONObject jsonObject = defaultSettings.getJSONObject(key);
        JSONArray jsonArray = jsonObject.getJSONArray(jsonArrayKey);
        List<String> result = new ArrayList<>();
        for (Object i : jsonArray) {
            result.add(i.toString());
        }
        return result;
    }

    @Override
    public void setDefaultSettings() {
        settings = defaultSettings;
    }

    @Override
    public void setSettingsFromFile(String filePath) {
        if (!doesFileSettingsExist(this.filePath)) {
            settings = defaultSettings;
        }

        StringBuffer fileContent = new StringBuffer();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                fileContent.append(line + "\n");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject fileContentAsJson = new JSONObject(fileContent.toString());

        if (isJSONValid(fileContentAsJson)) {
            settings = fileContentAsJson;
        } else {
            settings = defaultSettings;
        }
    }

    @Override
    public boolean isJSONValid(JSONObject json) {
        Set<String> keys = json.keySet();
        if (!keys.contains(KEY_ECHO_SHOULD_CONTAIN)) {
            return false;
        }

        if (!keys.contains(KEY_ECHO_SHOULD_NOT_CONTAIN)) {
            return false;
        }

        if (!keys.contains(KEY_ECHO_SHOULD_NOT_CONTAIN)) {
            return false;
        }

        if (!keys.contains(KEY_WINDOW_ALLWAYS_ON_TOP)) {
            return false;
        }

        if (!keys.contains(KEY_WINDOW_COLOR)) {
            return false;
        }
        //todo DO IT LATER
        return true;
    }

    @Override
    public void writeSettingsToFile(String filePath) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            writer.write(settings.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void writeCurrentSettingsToDefaultFile() {
        writeSettingsToFile(filePath);
    }

    @Override
    public JSONObject getDefaultSettings() {
        return defaultSettings;
    }

    @Override
    public JSONObject getSettings() {
        return settings;
    }

    @Override
    public void setSettingBoolean(String key, Boolean value) {
        settings.put(key, value);
    }

    @Override
    public void setSettingInteger(String key, Integer value) {
        settings.put(key, value);
    }

    @Override
    public void setSettingString(String key, String value) {
        settings.put(key, value);
    }

    @Override
    public void setSettingDouble(String key, Double value) {
        settings.put(key, value);
    }

    @Override
    public void setSettingJSONObject(String key, JSONObject value) {
        settings.put(key, value);
    }

    @Override
    public Object getSetting(String key) {
        return settings.get(key);
    }

    @Override
    public Boolean getSettingBoolean(String key) {
        return settings.getBoolean(key);
    }

    @Override
    public Integer getSettingInteger(String key) {
        return settings.getInt(key);
    }

    @Override
    public Double getSettingDouble(String key) {
        return settings.getDouble(key);
    }

    @Override
    public String getSettingString(String key) {
        return settings.getString(key);
    }

    @Override
    public void addView(SettingsObserver.Observer view) {
        if (!views.contains(view)) {
            views.add(view);
        }
    }

    @Override
    public void deleteView(SettingsObserver.Observer view) {
        if (views.contains(view)) {
            views.remove(view);
        }
    }

    @Override
    public void notifyAllViewsForNewSettings() {
        for (SettingsObserver.Observer view : views) {
            view.updateSettings();
        }
    }

    @Override
    public void notifyAllViewsForReloadModel() {
        for (SettingsObserver.Observer view : views) {
            view.reloadModel();
        }
    }

    public static Color getColorFromColorAsString(String colorString) {
        if (!colorString.contains("java.awt.Color[")) {
            return Color.BLACK;
        }
        String[] split = colorString.split("=");
        int colorR = 0;
        int colorG = 0;
        int colorB = 0;
        try {
            colorR = Integer.valueOf(split[1].split(",")[0]);
            colorG = Integer.valueOf(split[2].split(",")[0]);
            colorB = Integer.valueOf(split[3].split("]")[0]);
        } catch (Exception e) {
            System.out.println("Wrong color - set to black.");
        }
        return new Color(colorR, colorB, colorG);
    }
}
