package com.michalrys.fileecho.view.settings;

public interface SettingsObserver {
    interface Controller{
        void updateMainWindowAndModelFromCurrentSettings();

        void setTranslateMainWindowButtons(int deltaX);
    }

    interface Observer {
        void updateSettings();

        void reloadModel();
    }

    interface Obeservable {
        void addView(SettingsObserver.Observer view);

        void deleteView(SettingsObserver.Observer view);

        void notifyAllViewsForNewSettings();

        void notifyAllViewsForReloadModel();
    }

}
