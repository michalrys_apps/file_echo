package com.michalrys.fileecho.view.settings;

import java.awt.*;

public class ColorConversion {

    public static Color toAwtColor(javafx.scene.paint.Color colorJavaFx) {
        return new java.awt.Color((float) colorJavaFx.getRed(),
                (float) colorJavaFx.getGreen(),
                (float) colorJavaFx.getBlue(),
                (float) colorJavaFx.getOpacity());
    }

    public static Color toAwtColor(String colorAwtAsString) {
        if (!colorAwtAsString.contains("java.awt.Color[")) {
            return Color.BLACK;
        }
        String[] split = colorAwtAsString.split("=");
        int colorR = 0;
        int colorG = 0;
        int colorB = 0;
        try {
            colorR = Integer.valueOf(split[1].split(",")[0]);
            colorG = Integer.valueOf(split[2].split(",")[0]);
            colorB = Integer.valueOf(split[3].split("]")[0]);
        } catch (Exception e) {
            System.out.println("Wrong color - set to black.");
        }
        return new Color(colorR, colorG, colorB);
    }

    public static javafx.scene.paint.Color toJavaFXColor(Color colorAwt) {
        return javafx.scene.paint.Color.rgb(colorAwt.getRed(), colorAwt.getGreen(), colorAwt.getBlue());
    }

    public static javafx.scene.paint.Color toJavaFXColor(String colorAwtString) {
        Color colorAwt = toAwtColor(colorAwtString);
        return javafx.scene.paint.Color.rgb(colorAwt.getRed(), colorAwt.getGreen(), colorAwt.getBlue());
    }


    public static String toWebColor(javafx.scene.paint.Color color) {
        return String.format("#%02X%02X%02X",
                (int) (color.getRed() * 255),
                (int) (color.getGreen() * 255),
                (int) (color.getBlue() * 255));
    }


    public static String toWebColor(String colorAwtAsString) {
        Color colorAwt = toAwtColor(colorAwtAsString);
        javafx.scene.paint.Color color = toJavaFXColor(colorAwt);

        return String.format("#%02X%02X%02X",
                (int) (color.getRed() * 255),
                (int) (color.getGreen() * 255),
                (int) (color.getBlue() * 255));
    }
}
