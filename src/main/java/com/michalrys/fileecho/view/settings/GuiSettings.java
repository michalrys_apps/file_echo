package com.michalrys.fileecho.view.settings;

import org.json.JSONObject;

import java.awt.*;

public interface GuiSettings extends SettingsObserver.Obeservable {
    boolean doesFileSettingsExist(String filePath);

    void generateDefaultSettings();

    void setDefaultSettings();

    void setSettingsFromFile(String filePath);

    boolean isJSONValid(JSONObject json);

    void writeSettingsToFile(String filePath);

    void writeCurrentSettingsToDefaultFile();

    JSONObject getDefaultSettings();

    JSONObject getSettings();

    Object getSetting(String key);

    Boolean getSettingBoolean(String key);

    Integer getSettingInteger(String key);

    Double getSettingDouble(String key);

    String getSettingString(String key);

    void setSettingBoolean(String key, Boolean value);

    void setSettingInteger(String key, Integer value);

    void setSettingDouble(String key, Double value);

    void setSettingString(String key, String value);

    void setSettingJSONObject(String key, JSONObject value);

    static Color getColorFromColorAsString(String colorString) {

        if (!colorString.contains("java.awt.Color[")) {
            return Color.BLACK;
        }
        String[] split = colorString.split("=");
        int colorR = 0;
        int colorG = 0;
        int colorB = 0;
        try {
            colorR = Integer.valueOf(split[1].split(",")[0]);
            colorG = Integer.valueOf(split[2].split(",")[0]);
            colorB = Integer.valueOf(split[3].split("]")[0]);
        } catch (Exception e) {
            System.out.println("Wrong color - set to black.");
        }
        return new Color(colorR, colorB, colorG);

    }
}
