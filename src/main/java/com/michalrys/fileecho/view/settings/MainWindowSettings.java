package com.michalrys.fileecho.view.settings;

import java.util.ArrayList;
import java.util.List;

public class MainWindowSettings implements SettingsObserver.Obeservable {
    private List<SettingsObserver.Observer> views = new ArrayList<>();
    private String appStatus;

    public MainWindowSettings() {
    }

    public String getAppStatus() {
        return appStatus;
    }

    public void setAppStatus(String appStatus) {
        this.appStatus = appStatus;
    }

    @Override
    public void addView(SettingsObserver.Observer view) {
        if (!views.contains(view)) {
            views.add(view);
        }
    }

    @Override
    public void deleteView(SettingsObserver.Observer view) {
        if (views.contains(view)) {
            views.remove(view);
        }
    }

    @Override
    public void notifyAllViewsForNewSettings() {
        for (SettingsObserver.Observer view : views) {
            view.updateSettings();
        }
    }

    @Override
    public void notifyAllViewsForReloadModel() {
        for (SettingsObserver.Observer view : views) {
            view.reloadModel();
        }
    }
}
