package com.michalrys.fileecho;

import java.io.File;

public interface MVC {
    interface Controller {
        void runWithInputArgs();

        void runAfterDragAndDrop();

        void runAfterStartClick();

        void stopAfterStopClick();

        void pauseOn();

        void pauseOff();

        void clearStatus();

        void reload();

        void showAuthor();
    }

    interface View {
        void update();

        void runWithInputArgs();

        void runAfterDragAndDrop();

        void runAfterStartClick();

        void stopAfterStopClick();

        void pauseOn();

        void pauseOff();

        void clearStatus();

        void reload();

        void showAuthor();

        void setTitleByFile();

        void updateMainWindowAndModelFromCurrentSettings();
    }
}
