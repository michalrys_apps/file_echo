package com.michalrys.fileecho.controller;

import com.michalrys.fileecho.MVC;
import com.michalrys.fileecho.RunApp;
import com.michalrys.fileecho.model.FileEcho;
import com.michalrys.fileecho.view.MainWindowViewController;

public class FileEchoController implements MVC.Controller {
    private MainWindowViewController view;
    private FileEcho model;

    public FileEchoController(FileEcho model, MainWindowViewController view) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void runWithInputArgs() {
        if (RunApp.file != null) {
            model.setFile(RunApp.file);
            view.buttonStopStart.setText(view.BUTTON_STOP);

            // FIXME there could be a problem with crashing main thread
            model.startReading();
            view.setTitleByFile();
            view.appStatus.setText("input args -> listening");
            startThreadForUpdatingStatus(100, 50);
        } else {
            view.buttonStopStart.setText(view.BUTTON_START);
            view.buttonStopStart.setDisable(true);
            view.buttonPause.setDisable(true);
            view.buttonReload.setDisable(true);
            view.appStatus.setText("do nothing");
        }
    }

    @Override
    public void runAfterDragAndDrop() {
        view.buttonPause.setDisable(false);
        view.buttonStopStart.setDisable(false);
        view.buttonReload.setDisable(false);

        model.stopReading();
        model.setFile(RunApp.file);

        view.buttonStopStart.setText(view.BUTTON_STOP);
        view.setTitleByFile();
        view.appStatus.setText("dragged and dropped -> listening");
        //FIXME there could be a problem with crashing main thread
        model.startReading();
        startThreadForUpdatingStatus(100, 50);
    }

    @Override
    public void runAfterStartClick() {
        if (model.getFile() != null) {
            view.buttonStopStart.setText(view.BUTTON_STOP);
            view.buttonPause.setDisable(false);

            clearStatus();
            // FIXME there could be a problem with crashing main thread
            model.startReading();
            startThreadForUpdatingStatus(100, 50);
            view.appStatus.setText("started -> listening");
        } else {
            view.buttonStopStart.setText(view.BUTTON_START);
            view.buttonStopStart.setDisable(true);
        }
    }

    @Override
    public void stopAfterStopClick() {
        view.buttonStopStart.setText(view.BUTTON_START);
        view.buttonPause.setDisable(true);
        view.appStatus.setText("stopped");
        model.stopReading();
    }

    @Override
    public void pauseOn() {
        view.buttonPause.setText(view.BUTTON_PAUSE_ON);
        view.buttonStopStart.setDisable(true);
        view.buttonClear.setDisable(true);
        view.buttonSettings.setDisable(true);
        view.buttonReload.setDisable(true);
        view.buttonAuthor.setDisable(true);
        model.pauseReading(true);
        view.appStatus.setText("paused");
    }

    @Override
    public void pauseOff() {
        view.buttonPause.setText(view.BUTTON_PAUSE_OFF);
        view.buttonStopStart.setDisable(false);
        view.buttonClear.setDisable(false);
        view.buttonSettings.setDisable(false);
        view.buttonReload.setDisable(false);
        view.buttonAuthor.setDisable(false);
        model.pauseReading(false);
        view.appStatus.setText("listening");
    }

    @Override
    public void clearStatus() {
        model.clearStatus();
        model.notifyAllViews();
        if (view.appStatus.getText().contains("cleared")) {
        } else {
            view.appStatus.setText("cleared -> " + view.appStatus.getText());
        }
    }

    @Override
    public void reload() {
        stopAfterStopClick();

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        runAfterStartClick();
        view.appStatus.setText("reloaded -> listening");
    }

    @Override
    public void showAuthor() {
        stopAfterStopClick();

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        view.setStatus("\n\n\tAuthor: Michał Ryś\n\tE-mail: michalrys@gmail.com\n\tLicense: Freeware for EC-E employees.");
        view.appStatus.setText("stopped");
    }

    private void startThreadForUpdatingStatus(int startingUpDelayInMiliSec, int updateDelayInMilisec) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(startingUpDelayInMiliSec);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                while (model.isReading()) {
                    try {
                        Thread.sleep(updateDelayInMilisec);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    model.notifyAllViews();
                }
            }
        });
        thread.start();
    }
}
