package com.michalrys.fileecho.controller;

import com.michalrys.fileecho.RunApp;
import com.michalrys.fileecho.model.FileEcho;
import com.michalrys.fileecho.view.MainWindowViewController;
import com.michalrys.fileecho.view.settings.ColorConversion;
import com.michalrys.fileecho.view.settings.GuiSettings;
import com.michalrys.fileecho.view.settings.JSONSettings;
import com.michalrys.fileecho.view.settings.SettingsObserver;
import javafx.scene.control.TextArea;
import javafx.scene.text.Font;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SettingsController implements SettingsObserver.Controller {
    private final FileEcho model;
    private final MainWindowViewController mainWindowViewController;
    private final TextArea status;
    private GuiSettings settings;
    private int buttonsOffset;

    public SettingsController(FileEcho model, MainWindowViewController mainWindowViewController, TextArea status,
                              GuiSettings settings, int buttonsOffset) {
        this.model = model;
        this.mainWindowViewController = mainWindowViewController;
        this.status = status;
        this.settings = settings;
        this.buttonsOffset = buttonsOffset;
    }

    @Override
    public void updateMainWindowAndModelFromCurrentSettings() {
        Boolean settingsAlwaysOnTop = settings.getSettingBoolean(JSONSettings.KEY_WINDOW_ALLWAYS_ON_TOP);
        RunApp.mainWindowStage.setAlwaysOnTop(settingsAlwaysOnTop);
        String fontName = settings.getSettingString(JSONSettings.KEY_WINDOW_FONT_NAME);
        int fontSize = settings.getSettingInteger(JSONSettings.KEY_WINDOW_FONT_SIZE);
        status.setFont(new Font(fontName, fontSize));

        String fontColorAwtString = settings.getSettingString(JSONSettings.KEY_WINDOW_FONT_COLOR);
        String fontColorHex = ColorConversion.toWebColor(fontColorAwtString);
        status.setStyle("-fx-text-fill: " + fontColorHex + ";");

        model.setStatusLimit(settings.getSettingInteger(JSONSettings.KEY_STATUS_LINE_LIMIT));

        //window
        Integer windowW = settings.getSettingInteger(JSONSettings.KEY_WINDOW_WIDTH);
        Integer windowH = settings.getSettingInteger(JSONSettings.KEY_WINDOW_HEIGHT);
        RunApp.mainWindowStage.setWidth(windowW);
        status.setPrefWidth(windowW - 10);
        RunApp.mainWindowStage.setHeight(windowH);
        status.setPrefHeight(windowH - 33);

        String windowColorAwtString = settings.getSettingString(JSONSettings.KEY_WINDOW_COLOR);
        String windowColorHex = ColorConversion.toWebColor(windowColorAwtString);
        status.setStyle(status.getStyle() + "-fx-control-inner-background:" + windowColorHex + ";");

        // contain lists - should contain
        Object shouldContain = settings.getSetting(JSONSettings.KEY_ECHO_SHOULD_CONTAIN);
        JSONObject shouldContainJsonObject = new JSONObject(shouldContain.toString());

        String keyShouldContainActualList = settings.getSettingString(JSONSettings.KEY_ECHO_SHOULD_CONTAIN_ACTUAL);
        Object shouldContainActualList = shouldContainJsonObject.get(keyShouldContainActualList);
        JSONArray shouldContainActualListJsonArray = new JSONArray(shouldContainActualList.toString());

        List<String> shouldContainActualMarkedList = new ArrayList<>();
        for (Object o : shouldContainActualListJsonArray) {
            shouldContainActualMarkedList.add(o.toString());
        }

        model.setShouldContainList(shouldContainActualMarkedList);
        model.setShouldContainListName(keyShouldContainActualList);

//        System.out.println("shouldContainActualMarkedList --> " + shouldContainActualMarkedList);
//        System.out.println("keyShouldContainActualList --> " + keyShouldContainActualList);

        // contain lists - should not contain
        Object shouldNotContain = settings.getSetting(JSONSettings.KEY_ECHO_SHOULD_NOT_CONTAIN);
        JSONObject shouldNotContainJsonObject = new JSONObject(shouldNotContain.toString());

        String keyShouldNotContainActualList = settings.getSettingString(JSONSettings.KEY_ECHO_SHOULD_NOT_CONTAIN_ACTUAL);
        Object shouldNotContainActualList = shouldNotContainJsonObject.get(keyShouldNotContainActualList);
        JSONArray shouldNotContainActualListJsonArray = new JSONArray(shouldNotContainActualList.toString());

        List<String> shouldNotContainActualMarkedList = new ArrayList<>();
        for (Object o : shouldNotContainActualListJsonArray) {
            shouldNotContainActualMarkedList.add(o.toString());
        }

        model.setShouldNotContainList(shouldNotContainActualMarkedList);
        model.setShouldNotContainListName(keyShouldNotContainActualList);

//        System.out.println("shouldNotContainActualMarkedList --> " + shouldNotContainActualMarkedList);
//        System.out.println("keyShouldNotContainActualList --> " + keyShouldNotContainActualList);

        // buttons offset
        buttonsOffset = settings.getSettingInteger(JSONSettings.KEY_WINDOW_BUTTONS_OFFSET);
        setTranslateMainWindowButtons(buttonsOffset);
    }

    @Override
    public void setTranslateMainWindowButtons(int deltaX) {
        if (buttonsOffset != deltaX) {
            buttonsOffset = deltaX;
            settings.setSettingInteger(JSONSettings.KEY_WINDOW_BUTTONS_OFFSET, buttonsOffset);
        }

        mainWindowViewController.buttonAuthor.setTranslateX((double) deltaX);
        mainWindowViewController.appStatus.setTranslateX((double) deltaX);
        mainWindowViewController.buttonPause.setTranslateX((double) deltaX);
        mainWindowViewController.buttonStopStart.setTranslateX((double) deltaX);
        mainWindowViewController.buttonClear.setTranslateX((double) deltaX);
        mainWindowViewController.buttonReload.setTranslateX((double) deltaX);
        mainWindowViewController.buttonSettings.setTranslateX((double) deltaX);
        mainWindowViewController.buttonMin.setTranslateX((double) deltaX);
        mainWindowViewController.buttonClose.setTranslateX((double) deltaX);
    }
}
