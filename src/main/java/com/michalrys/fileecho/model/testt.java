package com.michalrys.fileecho.model;

import com.michalrys.fileecho.view.settings.ColorConversion;
import org.json.JSONObject;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class testt {
    public static void main(String[] args) {
        List<String> listText = new ArrayList<>();

        String text = "\"ala\"   , \"ma kota\",    \"pies\"";
        System.out.println(text);

        listText.addAll(Arrays.asList(text.split(",")));

        List<String> correctListText = new ArrayList<>();
        for(String word : listText) {
            word = word.replaceAll("[ ]*\"","\"");
            word = word.replaceAll("\"[ ]*","\"");
            word = word.replaceAll("\"", "");
            correctListText.add(word);
        }

        for(String word : correctListText) {
            System.out.println(">" + word + "<");
        }
    }
}
