package com.michalrys.fileecho.model;

import com.michalrys.fileecho.MVC;
import com.michalrys.fileecho.RunApp;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AnyFileEcho implements FileEcho {
    private List<MVC.View> views = new ArrayList<>();

    private File file;
    private StringBuffer status = new StringBuffer();
    private int statusLine = 0;
    private int statusLineLimit = 30;
    private boolean isReading;
    private boolean isPaused;

    private String shouldContainListName = "Should contain any word from list";
    private List<String> shouldContainList = Arrays.asList("a");
    private String shouldNotContainListName = "Should not contain any word from list";
    private List<String> shouldNotContainList = Arrays.asList("alamakota");

    @Override
    public void setFile(File file) {
        this.file = isFileValid(file) ? file : null;
        notifyAllViews();
    }

    @Override
    public File getFile() {
        return file;
    }

    @Override
    public void setFile(List<File> files) {
        if (files.size() != 0) {
            file = isFileValid(files.get(files.size() - 1)) ? files.get(files.size() - 1) : null;
        } else {
            file = null;
        }
    }

    @Override
    public synchronized void startReading() {
        Thread threadReading = new Thread(new Runnable() {
            @Override
            public void run() {
                status = new StringBuffer();
                statusLine = 0;
                String newLine;
                try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
                    isReading = true;

                    while (isReading) {
                        if (isPaused) {
                            pauseThread(1000);
                            continue;
                        }

                        newLine = bufferedReader.readLine();
                        if (newLine == null) {
                            pauseThread(500);
                            continue;
                        }
//                        status.append(newLine + "\n");
//                        statusLine++;
//                        reduceStatus();
                        addNewLineToStatusIfIsInShouldListAndIfIsNotInShouldNotList(newLine);
                    }
                    deleteNewLine();

                } catch (IOException e) {
                    status.append("!!! There was problem with reading file. !!!\n");
                    status.append(e.getStackTrace().toString() + "\n\n");
                    statusLine++;
                    reduceStatus();
                }
            }
        });

        threadReading.start();
    }

    private void addNewLineToStatusIfIsInShouldListAndIfIsNotInShouldNotList(String newLine) {
//        if (shouldNotContainList.size() != 1 || shouldNotContainList.get(0) != "") {
//            for (String shouldNotContainWord : shouldNotContainList) {
//                if (newLine.contains(shouldNotContainWord)) {
//                    return;
//                }
//            }
//        }

        for (String shouldContainWord : shouldContainList) {
            if (newLine.contains(shouldContainWord)) {
                status.append(newLine + "\n");
                statusLine++;
                reduceStatus();
            }
        }
    }

    @Override
    public void pauseReading(boolean isPaused) {
        this.isPaused = isPaused;
    }

    @Override
    public void stopReading() {
        this.isReading = false;
    }

    @Override
    public StringBuffer getStatus() {
        return status;
    }

    @Override
    public void clearStatus() {
        status = new StringBuffer();
    }

    private boolean isFileValid(File file) {
        return file.exists();
    }

    private void pauseThread(int milisec) {
        try {
            Thread.sleep(milisec);
        } catch (InterruptedException e) {
            status.append(e.getStackTrace().toString() + "\n\n");
        }
    }

    private void reduceStatus() {
        if (statusLine >= statusLineLimit) {
            int i = status.indexOf("\n");
            status.replace(0, i + 1, "");
            statusLine--;
        }
    }

    private void deleteNewLine() {
        status.replace(status.length() - 1, status.length(), "");
    }

    @Override
    public void addView(MVC.View view) {
        if (!views.contains(view)) {
            views.add(view);
        }
    }

    @Override
    public void deleteView(MVC.View view) {
        if (views.contains(view)) {
            views.remove(view);
        }
    }

    @Override
    public void notifyAllViews() {
        for (MVC.View view : views) {
            view.update();
        }
    }

    @Override
    public boolean isReading() {
        return isReading;
    }

    @Override
    public void setStatusLimit(int maxLines) {
        statusLineLimit = maxLines + 2;
    }

    @Override
    public int getStatusLimit() {
        return statusLineLimit + 2;
    }

    @Override
    public void setShouldContainListName(String shouldContainListName) {
        this.shouldContainListName = shouldContainListName;
    }

    @Override
    public String getShouldContainListName() {
        return shouldContainListName;
    }

    @Override
    public void setShouldContainList(List<String> shouldContainList) {
        this.shouldContainList = shouldContainList;
    }

    @Override
    public List<String> getShouldContainList() {
        return shouldContainList;
    }

    @Override
    public void setShouldNotContainListName(String shouldNotContainListName) {
        this.shouldNotContainListName = shouldNotContainListName;
    }

    @Override
    public String getShouldNotContainListName() {
        return shouldNotContainListName;
    }

    @Override
    public void setShouldNotContainList(List<String> shouldNotContainList) {
        this.shouldNotContainList = shouldNotContainList;
    }

    @Override
    public List<String> getShouldNotContainList() {
        return shouldNotContainList;
    }
}