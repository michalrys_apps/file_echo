package com.michalrys.fileecho.model;

import com.michalrys.fileecho.Observable;

import java.io.File;
import java.util.List;

public interface FileEcho extends Observable {
    void setFile(File file);

    void setFile(List<File> files);

    File getFile();

    void startReading();

    void pauseReading(boolean isPaused);

    void stopReading();

    StringBuffer getStatus();

    void clearStatus();

    boolean isReading();

    void setStatusLimit(int maxLines);

    int getStatusLimit();

    void setShouldContainListName(String shouldContainListName);

    String getShouldContainListName();

    void setShouldContainList(List<String> shouldContainList);

    List<String> getShouldContainList();

    void setShouldNotContainListName(String shouldNotContainListName);

    String getShouldNotContainListName();

    void setShouldNotContainList(List<String> shouldNotContainList);

    List<String> getShouldNotContainList();
}
