package com.michalrys.fileecho;

import com.michalrys.fileecho.MVC;

public interface Observable {
    void addView(MVC.View view);

    void deleteView(MVC.View view);

    void notifyAllViews();
}
