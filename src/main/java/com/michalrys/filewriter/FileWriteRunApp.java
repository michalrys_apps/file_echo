package com.michalrys.filewriter;

import com.michalrys.fileecho.RunApp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileWriteRunApp {
    public static String appPath;

    public static void main(String[] args) {
        setAppPath();


        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(appPath + "test.txt"))) {
            String text = getTestText();
            List<String> textList = new ArrayList<>();
            textList.addAll(Arrays.asList(text.split("\n")));

            while (true) {


                for (String line : textList) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    bufferedWriter.write(line + "\n");
                    bufferedWriter.flush();
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private static void setAppPath() {
        String path = "";
        try {
            path = new File(RunApp.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getPath();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        //String lastPart = path.replaceFirst(".*\\\\", "");
        String lastPart = path.replaceFirst(".*\\\\", "");

        String[] foldersSplitted = path.split("\\\\");

        appPath = "";
        for (int i = 0; i <= foldersSplitted.length - 2; i++) {
            appPath += foldersSplitted[i] + "\\";
        }
        //appPath = path.replaceFirst(lastPart, "") + "\\";
    }

    public static String getTestText() {
        return "1\n" +
                " \n" +
                " \n" +
                " \n" +
                " \n" +
                "                                              * * * * * * * * * * * * * * * * * * * *\n" +
                "                                              * * * * * * * * * * * * * * * * * * * *\n" +
                "                                              * * * * * * * * * * * * * * * * * * * *\n" +
                "1 News file -  (April 9, 2015):\n" +
                "  \n" +
                "               Welcome to MSC Nastran 2014.1\n" +
                "  \n" +
                "  \n" +
                "1                                                                               MAY  12, 2019  MSC Nastran  7/ 3/15   PAGE     1\n" +
                "                                                                                                                                    \n" +
                "0        N A S T R A N    F I L E    A N D    S Y S T E M    P A R A M E T E R    E C H O                                           \n" +
                "0                                                                                                                                   \n" +
                "                                                                                                                                    \n" +
                "                                                                                                                                    \n" +
                "     NASTRAN BUFFSIZE=65537 $(/MSC/MSC_NASTRAN/20141/CONF/NAST20141RC[4])            \n" +
                "     NASTRAN PARALLEL=8 $(/MSC/MSC_NASTRAN/20141/CONF/NAST20141RC[8])                \n" +
                "     $ $(/MSC/MSC_NASTRAN/20141/CONF/NAST20141RC[9])                                 \n" +
                "     $ $(/MSC/MSC_NASTRAN/20141/CONF/NAST20141RC[16])                                \n" +
                "     $ END $(/MSC/MSC_NASTRAN/20141/CONF/NAST20141RC[17])                            \n" +
                "     NASTRAN PARALLEL=8 $(COMMAND LINE[2])                                           \n" +
                "     $                                                                               \n" +
                "     $------------------------------------------------------------------------------$\n" +
                "     $ SOLUTION SETTINGS                                                            $\n" +
                "     $------------------------------------------------------------------------------$\n" +
                "     $                                                                               \n" +
                "1                                                                               MAY  12, 2019  MSC Nastran  7/ 3/15   PAGE     2\n" +
                "                                                                                                                                    \n" +
                "0        N A S T R A N    E X E C U T I V E    C O N T R O L    E C H O                                                             \n" +
                "0                                                                                                                                   \n" +
                "                                                                                                                                    \n" +
                "                                                                                                                                    \n" +
                "     SOL 101                                                                                 \n" +
                "     $                                                                                       \n" +
                "     GEOMCHECK NONE                                                                          \n" +
                "     $                                                                                       \n" +
                "     CEND                                                                                    \n" +
                "1                                                                               MAY  12, 2019  MSC Nastran  7/ 3/15   PAGE     3\n" +
                "                                                                                                                                    \n" +
                "0                                                                                                                                   \n" +
                "0                                        C A S E    C O N T R O L    E C H O                                                        \n" +
                "                 COMMAND                                                                                                            \n" +
                "                 COUNT                                                                                                              \n" +
                "                  1      $                                                                                       \n" +
                "                  2      $                                                                                       \n" +
                "                  3      $-----------------------------------------------------------------------        -------$\n" +
                "                  4      $ DEFINITION OF LOAD CASES                                                             $\n" +
                "                  5      $-----------------------------------------------------------------------        -------$\n" +
                "                  6      $                                                                                       \n" +
                "                  7      ECHO = NONE                                                                             \n" +
                "                  8      $                                                                                       \n" +
                "                  9      EQUILIBRIUM=YES                                                                         \n" +
                "                 10      $                                                                                       \n" +
                "                 11      $ ANALYSIS SETS                                                                         \n" +
                "                 12      SET 1 = 100000 THRU 130319,174561 THRU 174906                                           \n" +
                "                 13      $                                                                                       \n" +
                "                 14      $ OUTPUT REQUESTS                                                                       \n" +
                "                 15      DISPLACEMENT(PLOT,SORT1,REAL)=ALL                                                       \n" +
                "                 16      SPCFORCES(PRINT,SORT1,REAL)=ALL                                                         \n" +
                "                 17      MPCFORCES(PLOT,SORT1,REAL)=ALL                                                          \n" +
                "                 18      STRESS(PLOT,SORT1,REAL,VONMISES,BILIN)=1                                                \n" +
                "                 19      FORCE(PLOT,SORT1,REAL,BILIN)=ALL                                                        \n" +
                "                 20      BOUTPUT(PLOT,SORT1,REAL)=ALL                                                            \n" +
                "                 21      $                                                                                       \n" +
                "                 22      $ BCS FOR ALL LOAD CASES                                                                \n" +
                "                 23      SPC = 1000                                                                              \n" +
                "                 24      $                                                                                       \n" +
                "                 25      $ INITIAL CONTACT TABLE                                                                 \n" +
                "                 26      BCONTACT = 0                                                                            \n" +
                "                 27      $                                                                                       \n" +
                "                 28      $ CONTROL PARAMETERS                                                                    \n" +
                "                 29      NLSTEP = 1                                                                              \n" +
                " *** USER WARNING MESSAGE 601 (IFP1RULE)\n" +
                "     NLSTEP command is only supported in SOL 101 linear contact analysis and SOL 400. All other cases will be ignored.\n" +
                "                 30      $                                                                                       \n" +
                "                 31      $ LOAD CASES                                                                            \n" +
                "                 32      $                                                                                       \n" +
                "                 33      SUBCASE 101                                                                             \n" +
                "                 34          SUBTITLE=LC101_                                                       \n" +
                "                 35          LOAD=101                                                                            \n" +
                "                 36          BCONTACT=1                                                                          \n" +
                "                 37      $                                                                                       \n" +
                "                 38      SUBCASE 102                                                                             \n" +
                "                 39          SUBTITLE=LC102_                                                     \n" +
                "                 40          LOAD=102                                                                            \n" +
                "                 41          BCONTACT=1                                                                          \n" +
                "                 42      $                                                                                       \n" +
                "                 94      $                                                                                       \n" +
                "                 95      $-----------------------------------------------------------------------        -------$\n" +
                "                 96      $ BEGIN OF THE BULK DATA SECTION                                                       $\n" +
                "                 97      $-----------------------------------------------------------------------        -------$\n" +
                "                 98      $                                                                                       \n" +
                "                 99      BEGIN BULK                                                                              \n" +
                "1                                                                               MAY  12, 2019  MSC Nastran  7/ 3/15   PAGE     5\n" +
                "                                                                                                                                    \n" +
                "0                                                                                                                                   \n" +
                "                                              I N P U T   B U L K   D A T A   E C H O                                               \n" +
                "                 ENTRY                                                                                                              \n" +
                "                                                                                                                                    \n" +
                "0                                                                                                                                   \n" +
                "                                        M O D E L   S U M M A R Y          BULK = 0\n" +
                "                                   ENTRY NAME        NUMBER OF ENTRIES\n" +
                "                                   ----------        -----------------\n" +
                "                                      BCBODY1                        4\n" +
                "                                      BCONECT                        2\n" +
                "                                      BCONPRG                        1\n" +
                "                                       BCPARA                        1\n" +
                "                                       BCPROP                        4\n" +
                "                                      BCTABL1                        2\n" +
                "                                         CBAR                       33\n" +
                "                                        CHEXA                     6498\n" +
                "                                        CONM2                        1\n" +
                "                                       CPENTA                       90\n" +
                "                                       CQUAD4                     4975\n" +
                "                                       CTETRA                    62516\n" +
                "                                       CTRIA3                      215\n" +
                "                                       CTRIA6                    41403\n" +
                "                                         GRAV                        3\n" +
                "                                         GRID                   139797\n" +
                "                                         LOAD                       12\n" +
                "                                         MAT1                        9\n" +
                "                                       NLSTEP                        1\n" +
                "                                        PARAM                        5\n" +
                "                                        PBARL                        5\n" +
                "                                       PSHELL                       10\n" +
                "                                       PSOLID                        8\n" +
                "                                         RBE2                       61\n" +
                "                                         RBE3                        1\n" +
                "                                         SPC1                        1\n" +
                "\n" +
                " ^^^     \n" +
                " ^^^ >>> IFP OPERATIONS COMPLETE <<< \n" +
                " ^^^     \n" +
                " *** USER INFORMATION MESSAGE 3 (NONL_STS)\n" +
                " 1_MF.sts)\n" +
                " *** USER INFORMATION MESSAGE 4109 (OUTPX2)\n" +
                "     THE LABEL IS XXXXXXXX FOR FORTRAN UNIT 12\n" +
                "     (MAXIMUM SIZE OF FORTRAN RECORDS WRITTEN =          7 WORDS.)\n" +
                "           (NUMBER OF FORTRAN RECORDS WRITTEN =          8 RECORDS.)\n" +
                "                (TOTAL DATA WRITTEN FOR LABEL =         17 WORDS.)\n" +
                " *** USER INFORMATION MESSAGE 4114 (OUTPX2)\n" +
                "     DATA BLOCK GEOM1X   WRITTEN ON FORTRAN UNIT  12 IN BINARY (LTLEND) FORMAT USING NDDL DESCRIPTION FOR GEOM16, TRL =\n" +
                "                    101                 0                 0                 8                 0                 0                 0\n" +
                "     NAME OF DATA BLOCK WRITTEN ON FORTRAN UNIT IS GEOM1   \n" +
                "        (MAXIMUM POSSIBLE FORTRAN RECORD SIZE =      131074 WORDS.)\n" +
                "     (MAXIMUM SIZE OF FORTRAN RECORDS WRITTEN =      131074 WORDS.)\n" +
                "           (NUMBER OF FORTRAN RECORDS WRITTEN =          36 RECORDS.)\n" +
                "           (TOTAL DATA WRITTEN FOR DATA BLOCK =     1118418 WORDS.)\n" +
                "1                                                                               MAY  12, 2019  MSC Nastran  7/ 3/15   PAGE     7\n" +
                "                                                                                                                                    \n" +
                "0                                                                                                                                   \n" +
                "0                                                                                                                                   \n" +
                "                                                                                                                                    \n" +
                "                                                                                                                                    \n" +
                " *** USER INFORMATION MESSAGE 4114 (OUTPX2)\n" +
                "     DATA BLOCK GEOM2X   WRITTEN ON FORTRAN UNIT  12 IN BINARY (LTLEND) FORMAT USING NDDL DESCRIPTION FOR GEOM20, TRL =\n" +
                "                    102                 2               256               129              8736               128                 0\n" +
                "     NAME OF DATA BLOCK WRITTEN ON FORTRAN UNIT IS GEOM2   \n" +
                "        (MAXIMUM POSSIBLE FORTRAN RECORD SIZE =      131074 WORDS.)\n" +
                "     (MAXIMUM SIZE OF FORTRAN RECORDS WRITTEN =      131074 WORDS.)\n" +
                "           (NUMBER OF FORTRAN RECORDS WRITTEN =          75 RECORDS.)\n" +
                "           (TOTAL DATA WRITTEN FOR DATA BLOCK =     1505996 WORDS.)\n" +
                " *** USER INFORMATION MESSAGE 4114 (OUTPX2)\n" +
                "     DATA BLOCK GEOM3X   WRITTEN ON FORTRAN UNIT  12 IN BINARY (LTLEND) FORMAT USING NDDL DESCRIPTION FOR GEOM30, TRL =\n" +
                "                    103                 0                 0                16                 8                 0                 0\n" +
                "     NAME OF DATA BLOCK WRITTEN ON FORTRAN UNIT IS GEOM3   \n" +
                "        (MAXIMUM POSSIBLE FORTRAN RECORD SIZE =      131074 WORDS.)\n" +
                "     (MAXIMUM SIZE OF FORTRAN RECORDS WRITTEN =         123 WORDS.)\n" +
                "           (NUMBER OF FORTRAN RECORDS WRITTEN =          25 RECORDS.)\n" +
                "           (TOTAL DATA WRITTEN FOR DATA BLOCK =         182 WORDS.)\n" +
                " *** USER INFORMATION MESSAGE 4114 (OUTPX2)\n" +
                "     DATA BLOCK GEOM4X   WRITTEN ON FORTRAN UNIT  12 IN BINARY (LTLEND) FORMAT USING NDDL DESCRIPTION FOR GEOM470, TRL =\n" +
                "                    104                 0                 0                 0                64              2560                 0\n" +
                "     NAME OF DATA BLOCK WRITTEN ON FORTRAN UNIT IS GEOM4   \n" +
                "        (MAXIMUM POSSIBLE FORTRAN RECORD SIZE =      131074 WORDS.)\n" +
                "     (MAXIMUM SIZE OF FORTRAN RECORDS WRITTEN =        2879 WORDS.)\n" +
                "           (NUMBER OF FORTRAN RECORDS WRITTEN =          30 RECORDS.)\n" +
                "           (TOTAL DATA WRITTEN FOR DATA BLOCK =        3176 WORDS.)\n" +
                " *** USER INFORMATION MESSAGE 4114 (OUTPX2)\n" +
                "     DATA BLOCK EPTX     WRITTEN ON FORTRAN UNIT  12 IN BINARY (LTLEND) FORMAT USING NDDL DESCRIPTION FOR EPT0, TRL =\n" +
                "                    105                 0              4864                 0                 0                 0                32\n" +
                "     NAME OF DATA BLOCK WRITTEN ON FORTRAN UNIT IS EPT     \n" +
                "        (MAXIMUM POSSIBLE FORTRAN RECORD SIZE =      131074 WORDS.)\n" +
                "     (MAXIMUM SIZE OF FORTRAN RECORDS WRITTEN =         113 WORDS.)\n" +
                "           (NUMBER OF FORTRAN RECORDS WRITTEN =          35 RECORDS.)\n" +
                "           (TOTAL DATA WRITTEN FOR DATA BLOCK =         362 WORDS.)\n" +
                " *** USER INFORMATION MESSAGE 4114 (OUTPX2)\n" +
                "     DATA BLOCK MPTS     WRITTEN ON FORTRAN UNIT  12 IN BINARY (LTLEND) FORMAT USING NDDL DESCRIPTION FOR MPT, TRL =\n" +
                "                    101             32768                 6             65536                 0                 0                 0\n" +
                "     NAME OF DATA BLOCK WRITTEN ON FORTRAN UNIT IS MPTS    \n" +
                "        (MAXIMUM POSSIBLE FORTRAN RECORD SIZE =      131074 WORDS.)\n" +
                "     (MAXIMUM SIZE OF FORTRAN RECORDS WRITTEN =         279 WORDS.)\n" +
                "           (NUMBER OF FORTRAN RECORDS WRITTEN =          35 RECORDS.)\n" +
                "           (TOTAL DATA WRITTEN FOR DATA BLOCK =         789 WORDS.)\n" +
                " *** USER INFORMATION MESSAGE 4114 (OUTPX2)\n" +
                "     DATA BLOCK DYNAMICS WRITTEN ON FORTRAN UNIT  12 IN BINARY (LTLEND) FORMAT USING NDDL DESCRIPTION FOR DYNAMIC, TRL =\n" +
                "                    103              2048                 0                 0                 0               512                 0\n" +
                "     NAME OF DATA BLOCK WRITTEN ON FORTRAN UNIT IS DYNAMICS\n" +
                "        (MAXIMUM POSSIBLE FORTRAN RECORD SIZE =      131074 WORDS.)\n" +
                "     (MAXIMUM SIZE OF FORTRAN RECORDS WRITTEN =         111 WORDS.)\n" +
                "           (NUMBER OF FORTRAN RECORDS WRITTEN =          25 RECORDS.)\n" +
                "           (TOTAL DATA WRITTEN FOR DATA BLOCK =         221 WORDS.)\n" +
                " *** USER INFORMATION MESSAGE 4114 (OUTPX2)\n" +
                "     DATA BLOCK DIT      WRITTEN ON FORTRAN UNIT  12 IN BINARY (LTLEND) FORMAT USING NDDL DESCRIPTION FOR DI, TRL =\n" +
                "                    101                32                 0                 0                 0                 0                 0\n" +
                "     NAME OF DATA BLOCK WRITTEN ON FORTRAN UNIT IS DIT     \n" +
                "        (MAXIMUM POSSIBLE FORTRAN RECORD SIZE =      131074 WORDS.)\n" +
                "     (MAXIMUM SIZE OF FORTRAN RECORDS WRITTEN =         171 WORDS.)\n" +
                "           (NUMBER OF FORTRAN RECORDS WRITTEN =          20 RECORDS.)\n" +
                "           (TOTAL DATA WRITTEN FOR DATA BLOCK =         202 WORDS.)\n" +
                "1                                                                               MAY  12, 2019  MSC Nastran  7/ 3/15   PAGE     8\n" +
                "                                                                                                                                    \n" +
                "0                                                                                                                                   \n" +
                "0                                                                                                                                   \n" +
                "                                                                                                                                    \n" +
                "                                                                                                                                    \n" +
                " *** USER INFORMATION MESSAGE 4114 (OUTPX2)\n" +
                "     DATA BLOCK CONTACT  WRITTEN ON FORTRAN UNIT  12 IN BINARY (LTLEND) FORMAT USING NDDL DESCRIPTION FOR CONTAC, TRL =\n" +
                "                    101                32                 8                26               512                 0                 0\n" +
                "     NAME OF DATA BLOCK WRITTEN ON FORTRAN UNIT IS CONTACT \n" +
                "        (MAXIMUM POSSIBLE FORTRAN RECORD SIZE =      131074 WORDS.)\n" +
                "     (MAXIMUM SIZE OF FORTRAN RECORDS WRITTEN =         118 WORDS.)\n" +
                "           (NUMBER OF FORTRAN RECORDS WRITTEN =          45 RECORDS.)\n" +
                "           (TOTAL DATA WRITTEN FOR DATA BLOCK =         288 WORDS.)\n" +
                " *** USER INFORMATION MESSAGE 4114 (OUTPX2)\n" +
                "     DATA BLOCK OFCON3D0 WRITTEN ON FORTRAN UNIT  12 IN BINARY (LTLEND) FORMAT USING NDDL DESCRIPTION FOR OFCON3D, TRL =\n" +
                "                    102                 1                 4                 0                 3                10                 0\n" +
                "     NAME OF DATA BLOCK WRITTEN ON FORTRAN UNIT IS OFCON3D0\n" +
                "        (MAXIMUM POSSIBLE FORTRAN RECORD SIZE =      131074 WORDS.)\n" +
                "     (MAXIMUM SIZE OF FORTRAN RECORDS WRITTEN =       64224 WORDS.)\n" +
                "           (NUMBER OF FORTRAN RECORDS WRITTEN =          20 RECORDS.)\n" +
                "           (TOTAL DATA WRITTEN FOR DATA BLOCK =       64398 WORDS.)\n" +
                "1                                                                               MAY  12, 2019  MSC Nastran  7/ 3/15   PAGE     9\n" +
                "                                                                                                                                    \n" +
                "0                                                                                                                                   \n" +
                "                           O U T P U T   F R O M   G R I D   P O I N T   W E I G H T   G E N E R A T O R\n" +
                "0                                                     REFERENCE POINT =        0\n" +
                "                                                                M O\n" +
                "                      *  1.160666E-01  0.000000E+00  0.000000E+00  0.000000E+00  1.923491E+01 -1.211899E+02 *\n" +
                "                      *  0.000000E+00  1.160666E-01  0.000000E+00 -1.923491E+01  0.000000E+00 -2.294512E+02 *\n" +
                "                      *  0.000000E+00  0.000000E+00  1.160666E-01  1.211899E+02  2.294512E+02  0.000000E+00 *\n" +
                "                      *  0.000000E+00 -1.923491E+01  1.211899E+02  1.353775E+05  2.399413E+05  3.744358E+04 *\n" +
                "                      *  1.923491E+01  0.000000E+00  2.294512E+02  2.399413E+05  4.637952E+05 -1.864835E+04 *\n" +
                "                      * -1.211899E+02 -2.294512E+02  0.000000E+00  3.744358E+04 -1.864835E+04  5.840779E+05 *\n" +
                "                                                                 S\n" +
                "                                           *  1.000000E+00  0.000000E+00  0.000000E+00 *\n" +
                "                                           *  0.000000E+00  1.000000E+00  0.000000E+00 *\n" +
                "                                           *  0.000000E+00  0.000000E+00  1.000000E+00 *\n" +
                "                               DIRECTION\n" +
                "                          MASS AXIS SYSTEM (S)     MASS              X-C.G.        Y-C.G.        Z-C.G.\n" +
                "                                  X            1.160666E-01      0.000000E+00  1.044141E+03  1.657230E+02\n" +
                "                                  Y            1.160666E-01     -1.976892E+03  0.000000E+00  1.657230E+02\n" +
                "                                  Z            1.160666E-01     -1.976892E+03  1.044141E+03  0.000000E+00\n" +
                "                                                                I(S)\n" +
                "                                           *  5.650391E+03 -3.618939E+02  5.817555E+02 *\n" +
                "                                           * -3.618939E+02  7.007318E+03 -1.435608E+03 *\n" +
                "                                           *  5.817555E+02 -1.435608E+03  3.938288E+03 *\n" +
                "                                                                I(Q)\n" +
                "                                           *  5.826762E+03                             *\n" +
                "                                           *                3.186787E+03               *\n" +
                "                                           *                              7.582449E+03 *\n" +
                "                                                                 Q\n" +
                "                                           * -9.621131E-01  2.642256E-01  6.725431E-02 *\n" +
                "                                           * -3.401993E-02 -3.610789E-01  9.319145E-01 *\n" +
                "                                           *  2.705198E-01  8.943192E-01  3.563877E-01 *\n" +
                "\n" +
                "1                                                                               MAY  12, 2019  MSC Nastran  7/ 3/15   PAGE    10\n" +
                "                                                                                                                                    \n" +
                "0                                                                                                                                   \n" +
                "1                                                                               MAY  12, 2019  MSC Nastran  7/ 3/15   PAGE    11\n" +
                "                                                                                                                                    \n" +
                "0                                                                                                                                   \n" +
                "0 RESULTANTS ABOUT ORIGIN OF SUPERELEMENT BASIC COORDINATE SYSTEM IN SUPERELEMENT BASIC SYSTEM COORDINATES.\n" +
                "\n" +
                "0                                                  OLOAD    RESULTANT       \n" +
                "  SUBCASE/    LOAD\n" +
                "  DAREA ID    TYPE       T1            T2            T3            R1            R2            R3\n" +
                "0      101     FX    3.415841E+03     ----          ----          ----       5.660834E+05 -3.566620E+06                             \n" +
                "               FY       ----       0.000000E+00     ----       0.000000E+00     ----       0.000000E+00                             \n" +
                "               FZ       ----          ----       1.138614E+03  1.188873E+06  2.250916E+06     ----                                  \n" +
                "               MX       ----          ----          ----       0.000000E+00     ----          ----                                  \n" +
                "               MY       ----          ----          ----          ----       0.000000E+00     ----                                  \n" +
                "               MZ       ----          ----          ----          ----          ----       0.000000E+00                             \n" +
                "             TOTALS  3.415841E+03  0.000000E+00  1.138614E+03  1.188873E+06  2.817000E+06 -3.566620E+06\n" +
                "0      102     FX   -3.415841E+03     ----          ----          ----      -5.660834E+05  3.566620E+06                             \n" +
                "               FY       ----       0.000000E+00     ----       0.000000E+00     ----       0.000000E+00                             \n" +
                "               FZ       ----          ----       1.138614E+03  1.188873E+06  2.250916E+06     ----                                  \n" +
                "               MX       ----          ----          ----       0.000000E+00     ----          ----                                  \n" +
                "               MY       ----          ----          ----          ----       0.000000E+00     ----                                  \n" +
                "               MZ       ----          ----          ----          ----          ----       0.000000E+00                             \n" +
                "             TOTALS -3.415841E+03  0.000000E+00  1.138614E+03  1.188873E+06  1.684833E+06  3.566620E+06\n" +
                "0      103     FX    0.000000E+00     ----          ----          ----       0.000000E+00  0.000000E+00                             \n" +
                "               FY       ----       1.457426E+04     ----      -2.415289E+06     ----      -2.881173E+07                             \n" +
                "               FZ       ----          ----       1.138614E+03  1.188873E+06  2.250916E+06     ----                                  \n" +
                "               MX       ----          ----          ----       0.000000E+00     ----          ----                                  \n" +
                "               MY       ----          ----          ----          ----       0.000000E+00     ----                                  \n" +
                "               MZ       ----          ----          ----          ----          ----       0.000000E+00                             \n" +
                "             TOTALS  0.000000E+00  1.457426E+04  1.138614E+03 -1.226416E+06  2.250916E+06 -2.881173E+07\n" +
                "0      104     FX    0.000000E+00     ----          ----          ----       0.000000E+00  0.000000E+00                             \n" +
                "               FY       ----      -1.457426E+04     ----       2.415289E+06     ----       2.881173E+07                             \n" +
                "               FZ       ----          ----       1.138614E+03  1.188873E+06  2.250916E+06     ----                                  \n" +
                "               MX       ----          ----          ----       0.000000E+00     ----          ----                                  \n" +
                "               MY       ----          ----          ----          ----       0.000000E+00     ----                                  \n" +
                "               MZ       ----          ----          ----          ----          ----       0.000000E+00                             \n" +
                "             TOTALS  0.000000E+00 -1.457426E+04  1.138614E+03  3.604162E+06  2.250916E+06  2.881173E+07\n" +
                "0      105     FX    0.000000E+00     ----          ----          ----       0.000000E+00  0.000000E+00                             \n" +
                "               FY       ----       0.000000E+00     ----       0.000000E+00     ----       0.000000E+00                             \n" +
                "               FZ       ----          ----       3.028713E+04  3.162403E+07  5.987438E+07     ----                                  \n" +
                "               MX       ----          ----          ----       0.000000E+00     ----          ----                                  \n" +
                "               MY       ----          ----          ----          ----       0.000000E+00     ----                                  \n" +
                "               MZ       ----          ----          ----          ----          ----       0.000000E+00                             \n" +
                "             TOTALS  0.000000E+00  0.000000E+00  3.028713E+04  3.162403E+07  5.987438E+07  0.000000E+00\n" +
                "0      106     FX    0.000000E+00     ----          ----          ----       0.000000E+00  0.000000E+00                             \n" +
                "               FY       ----       0.000000E+00     ----       0.000000E+00     ----       0.000000E+00                             \n" +
                "               FZ       ----          ----      -2.800990E+04 -2.924628E+07 -5.537254E+07     ----                                  \n" +
                "               MX       ----          ----          ----       0.000000E+00     ----          ----                                  \n" +
                "               MY       ----          ----          ----          ----       0.000000E+00     ----                                  \n" +
                "               MZ       ----          ----          ----          ----          ----       0.000000E+00                             \n" +
                "             TOTALS  0.000000E+00  0.000000E+00 -2.800990E+04 -2.924628E+07 -5.537254E+07  0.000000E+00\n" +
                "0      201     FX    2.846534E+03     ----          ----          ----       4.717361E+05 -2.972183E+06                             \n" +
                "               FY       ----       0.000000E+00     ----       0.000000E+00     ----       0.000000E+00                             \n" +
                "               FZ       ----          ----       1.138614E+03  1.188873E+06  2.250916E+06     ----                                  \n" +
                "               MX       ----          ----          ----       0.000000E+00     ----          ----                                  \n" +
                "               MY       ----          ----          ----          ----       0.000000E+00     ----                                  \n" +
                "               MZ       ----          ----          ----          ----          ----       0.000000E+00                             \n" +
                "             TOTALS  2.846534E+03  0.000000E+00  1.138614E+03  1.188873E+06  2.722652E+06 -2.972183E+06\n" +
                "0      202     FX   -2.846534E+03     ----          ----          ----      -4.717361E+05  2.972183E+06                             \n" +
                "               FY       ----       0.000000E+00     ----       0.000000E+00     ----       0.000000E+00                             \n" +
                "               FZ       ----          ----       1.138614E+03  1.188873E+06  2.250916E+06     ----                                  \n" +
                "               MX       ----          ----          ----       0.000000E+00     ----          ----                                  \n" +
                "               MY       ----          ----          ----          ----       0.000000E+00     ----                                  \n" +
                "               MZ       ----          ----          ----          ----          ----       0.000000E+00                             \n" +
                "             TOTALS -2.846534E+03  0.000000E+00  1.138614E+03  1.188873E+06  1.779180E+06  2.972183E+06\n" +
                "0      203     FX    0.000000E+00     ----          ----          ----       0.000000E+00  0.000000E+00                             \n" +
                "               FY       ----       7.287128E+03     ----      -1.207644E+06     ----      -1.440586E+07                             \n" +
                "               FZ       ----          ----       1.138614E+03  1.188873E+06  2.250916E+06     ----                                  \n" +
                "               MX       ----          ----          ----       0.000000E+00     ----          ----                                  \n" +
                "               MY       ----          ----          ----          ----       0.000000E+00     ----                                  \n" +
                "               MZ       ----          ----          ----          ----          ----       0.000000E+00                             \n" +
                "             TOTALS  0.000000E+00  7.287128E+03  1.138614E+03 -1.877115E+04  2.250916E+06 -1.440586E+07\n" +
                "0      204     FX    0.000000E+00     ----          ----          ----       0.000000E+00  0.000000E+00                             \n" +
                "               FY       ----      -7.287128E+03     ----       1.207644E+06     ----       1.440586E+07                             \n" +
                "               FZ       ----          ----       1.138614E+03  1.188873E+06  2.250916E+06     ----                                  \n" +
                "               MX       ----          ----          ----       0.000000E+00     ----          ----                                  \n" +
                "               MY       ----          ----          ----          ----       0.000000E+00     ----                                  \n" +
                "               MZ       ----          ----          ----          ----          ----       0.000000E+00                             \n" +
                "             TOTALS  0.000000E+00 -7.287128E+03  1.138614E+03  2.396518E+06  2.250916E+06  1.440586E+07\n" +
                "0      205     FX    0.000000E+00     ----          ----          ----       0.000000E+00  0.000000E+00                             \n" +
                "               FY       ----       0.000000E+00     ----       0.000000E+00     ----       0.000000E+00                             \n" +
                "               FZ       ----          ----       9.883167E+03  1.031942E+07  1.953795E+07     ----                                  \n" +
                "               MX       ----          ----          ----       0.000000E+00     ----          ----                                  \n" +
                "               MY       ----          ----          ----          ----       0.000000E+00     ----                                  \n" +
                "               MZ       ----          ----          ----          ----          ----       0.000000E+00                             \n" +
                "             TOTALS  0.000000E+00  0.000000E+00  9.883167E+03  1.031942E+07  1.953795E+07  0.000000E+00\n" +
                "0      206     FX    0.000000E+00     ----          ----          ----       0.000000E+00  0.000000E+00                             \n" +
                "               FY       ----       0.000000E+00     ----       0.000000E+00     ----       0.000000E+00                             \n" +
                "               FZ       ----          ----      -7.605940E+03 -7.941674E+06 -1.503612E+07     ----                                  \n" +
                "               MX       ----          ----          ----       0.000000E+00     ----          ----                                  \n" +
                "               MY       ----          ----          ----          ----       0.000000E+00     ----                                  \n" +
                "               MZ       ----          ----          ----          ----          ----       0.000000E+00                             \n" +
                "             TOTALS  0.000000E+00  0.000000E+00 -7.605940E+03 -7.941674E+06 -1.503612E+07  0.000000E+00\n" +
                "1                                                                               MAY  12, 2019  MSC Nastran  7/ 3/15   PAGE    12\n" +
                "                                                                                                                                    \n" +
                "0                                                                                                            SUBCASE 101            \n" +
                " *** USER INFORMATION MESSAGE 7998 (NL3INT)\n" +
                "     Minimum Iteration default value is set to 2 because this job involves contact analysis, or you have set only \n" +
                "     \"U\" as convergence criteria.\n" +
                "1                                                                               MAY  12, 2019  MSC Nastran  7/ 3/15   PAGE    13\n" +
                "                                                                                                                                    \n" +
                "0                                                                                                            SUBCASE 101            \n" +
                "                       N O N - L I N E A R   I T E R A T I O N   S O L U T I O N   C O N T R O L   P A R A M E T E R S\n" +
                "\n" +
                "     LOOP CONTROLS FOR :   SUBCASE      101,         STEP        0,         SUBSTEP        0\n" +
                "\n" +
                "\n" +
                "     SOLUTION CONTROL PARAMETERS FROM : NLPARM                     ID :   10000001\n" +
                "\n" +
                "          Number of Increments (NINC) .............            1\n" +
                "          Incremental Time for Creep (DT) .........     0.00E+00\n" +
                "          Matrix Update Option (KMETHOD) ..........         AUTO\n" +
                "          Matrix Update Increment (KSTEP) .........          500\n" +
                "          Maximum Number of Iterations (MAXITER) ..           25\n" +
                "          Convergence Options (CONV) ..............     UP  V   \n" +
                "          Intermediate Output Flag (INTOUT) .......           NO\n" +
                "                     - Displacement   (EPSU) ......     1.00E-02\n" +
                "          Tolerance  - Residual Force (EPSP) ......     1.00E-02\n" +
                "                     - Work           (EPSW) ......     1.00E-02\n" +
                "          Divergence Limit (MAXDIV) ...............            5\n" +
                "          Maximum Quasi-Newton Vectors (MAXQN) ....            0\n" +
                "          Maximum Line Searches (MAXLS) ...........            0\n" +
                "          Error Tolerance in YF (FSTRESS) .........     2.00E-01\n" +
                "          Line Search Tolerance (LSTOL) ...........     5.00E-01\n" +
                "          Maximum Number of Bisection (MAXBIS) ....            5\n" +
                "          Maximum Ratio for Arc-Length (MAXR) .....     2.00E+01\n" +
                "          Maximum Incremental Rotation (RTOLB) ....     2.00E+01\n" +
                "          Minimum Number of Iterations (MINITER) ..            2\n" +
                "\n" +
                " *** USER INFORMATION MESSAGE 6204 (NL3EMA)\n" +
                "     5.321000E+01 SECONDS REQUIRED TO DECOMPOSE MATRIX.\n" +
                "1                                                                               MAY  12, 2019  MSC Nastran  7/ 3/15   PAGE    14\n" +
                "                                                                                                                                    \n" +
                "0                                                                                                            SUBCASE 101            \n" +
                "0                             N O N - L I N E A R   I T E R A T I O N   M O D U L E   O U T P U T\n" +
                "\n" +
                "     STIFFNESS UPDATE TIME-1892159974.85 SECONDS                                        SUBCASE      101          STEP        0\n" +
                "            ITERATION TIME         62.06 SECONDS\n" +
                "\n" +
                "        LOAD  NO.       - - ERROR FACTORS - -    CONV  ITR MAT NO.    AVG     TOTL       - - - - - DISP - - - - - -  LINE_S  NO. TOT TOT\n" +
                "        STEP  INC ITR  DISP     LOAD      WORK   RATE  DIV DIV BIS  R_FORCE   WORK      AVG        MAX     AT GRID C FACT NO QNV KUD ITR\n" +
                "\n" +
                "%1.00000E+00   1  1 1.00E+00 8.32E-02 1.00E+00  1.000  0   1   0  4.00E-02 1.344E+02 1.91E-02  2.374E-01   110223 1  1.00 0  0   0    1\n" +
                "%1.00000E+00   1  2 1.54E-02 3.44E-02 1.66E-03  0.414  0   1   0  1.29E-02 1.346E+02 1.91E-02  2.378E-01   110223 1  1.00 0  0   0    2\n" +
                "%1.00000E+00   1  3 5.14E-02 4.19E-02 2.30E-03  0.649  0   1   0  9.10E-03 1.349E+02 1.92E-02  2.382E-01   110223 1  1.00 0  0   0    3\n" +
                "%1.00000E+00   1  4 4.77E-02 3.12E-02 2.93E-03  0.796  0   1   0  7.70E-03 1.353E+02 1.92E-02  2.389E-01   110223 1  1.00 0  0   0    4\n" +
                "%1.00000E+00   1  5 9.82E-02 4.02E-02 4.19E-03  0.798  0   1   0  9.62E-03 1.359E+02 1.93E-02  2.397E-01   110223 1  1.00 0  0   0    5\n" +
                "%1.00000E+00   1  6 5.43E-02 4.33E-02 2.96E-03  0.817  0   1   0  4.37E-03 1.362E+02 1.93E-02  2.404E-01   110223 1  1.00 0  0   0    6\n" +
                "%1.00000E+00   1  7 4.09E-02 2.65E-02 1.71E-03  0.784  0   1   0  3.19E-03 1.365E+02 1.94E-02  2.409E-01   110223 1  1.00 0  0   0    7\n" +
                "%1.00000E+00   1  8 4.47E-02 2.81E-02 1.41E-03  0.778  0   1   0  1.86E-03 1.367E+02 1.94E-02  2.414E-01   110223 1  1.00 0  0   0    8\n" +
                "%1.00000E+00   1  9 3.96E-02 1.66E-02 6.84E-04  0.782  0   1   0  1.15E-03 1.368E+02 1.95E-02  2.417E-01   110223 1  1.00 0  0   0    9\n" +
                "%1.00000E+00   1 10 3.43E-02 8.83E-03 5.40E-04  0.569  0   1   0  8.00E-04 1.368E+02 1.95E-02  2.419E-01   110223 1  1.00 0  0   0   10\n" +
                "%1.00000E+00   1 11 4.70E-02 1.16E-02 4.27E-04  0.780  0   1   0  4.28E-04 1.369E+02 1.95E-02  2.421E-01   110223 1  1.00 0  0   0   11\n" +
                "%1.00000E+00   1 12 4.70E-02 8.60E-03 2.16E-04  0.799  0   1   0  2.55E-04 1.369E+02 1.95E-02  2.422E-01   110223 1  1.00 0  0   0   12\n" +
                "%1.00000E+00   1 13 1.46E-02 1.04E-02 5.83E-05  0.794  0   1   0  1.89E-04 1.369E+02 1.95E-02  2.423E-01   110223 1  1.00 0  0   0   13\n" +
                "%1.00000E+00   1 14 2.83E-02 2.42E-03 8.74E-05  0.667  2   1   0  5.53E-05 1.369E+02 1.95E-02  2.423E-01   110223 1  1.00 0  0   0   14\n" +
                "%1.00000E+00   1 15 3.02E-02 1.43E-03 4.94E-05  0.301  0   1   0  4.03E-05 1.369E+02 1.95E-02  2.423E-01   110223 1  1.00 0  0   0   15\n" +
                "%1.00000E+00   1 16 1.19E-03 1.61E-05 1.13E-06  0.375  0   1   0  1.97E-06 1.369E+02 1.95E-02  2.423E-01   110223 1  1.00 0  0   0   16\n" +
                "\n" +
                " *** JOB CONVERGES FOR THE CURRENT STEP.\n" +
                " *** SUBCASE    101 STEP      0 IS COMPLETED.\n" +
                "\n" +
                " *** USER INFORMATION MESSAGE 7998 (NL3INT)\n" +
                "     Minimum Iteration default value is set to 2 because this job involves contact analysis, or you have set only \n" +
                "     \"U\" as convergence criteria.\n" +
                "1                                                                               MAY  12, 2019  MSC Nastran  7/ 3/15   PAGE    15\n" +
                "                                                                                                                                    \n" +
                "0                                                                                                            SUBCASE 101            \n" +
                "                       N O N - L I N E A R   I T E R A T I O N   S O L U T I O N   C O N T R O L   P A R A M E T E R S\n" +
                "\n" +
                "     LOOP CONTROLS FOR :   SUBCASE      102,         STEP        0,         SUBSTEP        0\n" +
                "\n" +
                "\n" +
                "     SOLUTION CONTROL PARAMETERS FROM : NLPARM                     ID :   10000002\n" +
                "\n" +
                "          Number of Increments (NINC) .............            1\n" +
                "          Incremental Time for Creep (DT) .........     0.00E+00\n" +
                "          Matrix Update Option (KMETHOD) ..........         AUTO\n" +
                "          Matrix Update Increment (KSTEP) .........          500\n" +
                "          Maximum Number of Iterations (MAXITER) ..           25\n" +
                "          Convergence Options (CONV) ..............     UP  V   \n" +
                "          Intermediate Output Flag (INTOUT) .......           NO\n" +
                "                     - Displacement   (EPSU) ......     1.00E-02\n" +
                "          Tolerance  - Residual Force (EPSP) ......     1.00E-02\n" +
                "                     - Work           (EPSW) ......     1.00E-02\n" +
                "          Divergence Limit (MAXDIV) ...............            5\n" +
                "          Maximum Quasi-Newton Vectors (MAXQN) ....            0\n" +
                "          Maximum Line Searches (MAXLS) ...........            0\n" +
                "          Error Tolerance in YF (FSTRESS) .........     2.00E-01\n" +
                "          Line Search Tolerance (LSTOL) ...........     5.00E-01\n" +
                "          Maximum Number of Bisection (MAXBIS) ....            5\n" +
                "          Maximum Ratio for Arc-Length (MAXR) .....     2.00E+01\n" +
                "          Maximum Incremental Rotation (RTOLB) ....     2.00E+01\n" +
                "          Minimum Number of Iterations (MINITER) ..            2\n" +
                "\n" +
                " *** USER INFORMATION MESSAGE 6204 (NL3EMA)\n" +
                "     5.236000E+01 SECONDS REQUIRED TO DECOMPOSE MATRIX.\n" +
                "1                                                                               MAY  12, 2019  MSC Nastran  7/ 3/15   PAGE    16\n" +
                "                                                                                                                                    \n" +
                "0                                                                                                            SUBCASE 101            \n" +
                "0                             N O N - L I N E A R   I T E R A T I O N   M O D U L E   O U T P U T\n" +
                "\n" +
                "     STIFFNESS UPDATE TIME-1892159050.57 SECONDS                                        SUBCASE      102          STEP        0\n" +
                "            ITERATION TIME         61.35 SECONDS\n" +
                "\n" +
                "        LOAD  NO.       - - ERROR FACTORS - -    CONV  ITR MAT NO.    AVG     TOTL       - - - - - DISP - - - - - -  LINE_S  NO. TOT TOT\n" +
                "        STEP  INC ITR  DISP     LOAD      WORK   RATE  DIV DIV BIS  R_FORCE   WORK      AVG        MAX     AT GRID C FACT NO QNV KUD ITR\n" +
                "\n" +
                "%1.00000E+00   1  1 1.00E+00 5.39E-02 1.00E+00  1.000  0   1   0  3.60E-02 6.048E+01 1.29E-02  6.182E-01   110200 3  1.00 0  0   0    1\n" +
                "%1.00000E+00   1  2 6.04E-03 4.15E-02 3.91E-03  0.769  0   1   0  1.54E-02 6.071E+01 1.29E-02  6.197E-01   110200 3  1.00 0  0   0    2\n" +
                "%1.00000E+00   1  3 1.87E-02 4.53E-02 7.71E-03  0.791  0   1   0  1.35E-02 6.117E+01 1.29E-02  6.207E-01   110200 3  1.00 0  0   0    3\n" +
                "%1.00000E+00   1  4 7.26E-02 4.88E-02 1.82E-02  0.808  0   1   0  1.34E-02 6.230E+01 1.30E-02  6.214E-01   110200 3  1.00 0  0   0    4\n" +
                "%1.00000E+00   1  5 1.52E-01 7.47E-02 2.68E-02  0.831  0   1   0  1.41E-02 6.400E+01 1.30E-02  6.219E-01   110200 3  1.00 0  0   0    5\n" +
                "%1.00000E+00   1  6 4.25E-02 5.25E-02 2.04E-02  0.803  0   1   0  1.45E-02 6.531E+01 1.31E-02  6.224E-01   110200 3  1.00 0  0   0    6\n" +
                "%1.00000E+00   1  7 4.18E-02 4.46E-02 1.56E-02  0.764  0   1   0  6.48E-03 6.633E+01 1.32E-02  6.227E-01   110200 3  1.00 0  0   0    7\n";
    }
}